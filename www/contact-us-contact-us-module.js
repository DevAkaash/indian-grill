(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["contact-us-contact-us-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/contact-us/contact-us.page.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/contact-us/contact-us.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button meni=\"m1\">\n        <ion-icon name=\"menu\"></ion-icon>\n      </ion-menu-button>\n    </ion-buttons>\n    <ion-title>Contact Us</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size-xl=\"6\" offset-xl=\"3\">\n        <div>\n          <img src=\"https://3kpnuxym9k04c8ilz2quku1czd-wpengine.netdna-ssl.com/wp-content/uploads/2013/10/Screen-shot-2013-10-27-at-10.51.49-PM.png\" />\n        </div>\n        <h2 class=\"cssH2\">Contact Us</h2>\n        <div>\n          <form #contactUsForm=\"ngForm\" (ngSubmit)=\"onSubmit()\">\n            <ion-list>\n              <ion-item lines=\"none\">\n                <ion-label class=\"cssLabelText\" position=\"stacked\">Name :</ion-label>\n                <ion-input type=\"text\" ngModel name=\"name\" required></ion-input>\n              </ion-item>\n              <ion-item lines=\"none\">\n                <ion-label class=\"cssLabelText\" position=\"stacked\">Email :</ion-label>\n                <ion-input type=\"email\" ngModel name=\"email\" pattern=\"^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,8}$\" required></ion-input>\n              </ion-item>\n              <ion-item lines=\"none\">\n                <ion-label class=\"cssLabelText\" position=\"stacked\">Telephone :</ion-label>\n                <ion-input type=\"tel\" ngModel name=\"telephone\" minlength=\"10\" maxlength=\"10\" required></ion-input>\n              </ion-item>\n              <ion-item lines=\"none\">\n                <ion-label class=\"cssLabelText\" position=\"stacked\">Comment :</ion-label>\n                <ion-textarea rows=\"4\" ngModel name=\"comment\" required></ion-textarea>\n              </ion-item>\n              <ion-item lines=\"none\">\n                <ion-button class=\"cssButtonSubmit\" type=\"submit\" [disabled]=\"!contactUsForm.valid || onCheckValid()\">\n                  Send\n                </ion-button>\n              </ion-item>\n            </ion-list>\n          </form>\n        </div>\n        <div>\n          <div>\n            <img\n              src=\"https://www.indian-grill.com/wp-content/uploads/2016/11/IMG_0118.jpg\"\n            />\n          </div>\n          <div style=\"margin-top: 12px;\">\n            <p>\n              <strong>Indain Grill</strong>\n              <br>\n              <strong>HotBreads Cakes & Curries</strong>\n              <br>\n              <strong>969 Bethlehem Pike</strong>\n              <br>\n              <strong>Montgomeryville PA 18936</strong>\n            </p>\n            <p>Phone: 215-855-4900</p>\n            <p>E-mail: contact@indian-grill.com</p>\n            <p>Website: indian-grill.com</p>\n          </div>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/contact-us/contact-us-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/contact-us/contact-us-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: ContactUsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactUsPageRoutingModule", function() { return ContactUsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _contact_us_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./contact-us.page */ "./src/app/contact-us/contact-us.page.ts");




var routes = [
    {
        path: '',
        component: _contact_us_page__WEBPACK_IMPORTED_MODULE_3__["ContactUsPage"]
    }
];
var ContactUsPageRoutingModule = /** @class */ (function () {
    function ContactUsPageRoutingModule() {
    }
    ContactUsPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], ContactUsPageRoutingModule);
    return ContactUsPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/contact-us/contact-us.module.ts":
/*!*************************************************!*\
  !*** ./src/app/contact-us/contact-us.module.ts ***!
  \*************************************************/
/*! exports provided: ContactUsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactUsPageModule", function() { return ContactUsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _contact_us_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./contact-us-routing.module */ "./src/app/contact-us/contact-us-routing.module.ts");
/* harmony import */ var _contact_us_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./contact-us.page */ "./src/app/contact-us/contact-us.page.ts");







var ContactUsPageModule = /** @class */ (function () {
    function ContactUsPageModule() {
    }
    ContactUsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _contact_us_routing_module__WEBPACK_IMPORTED_MODULE_5__["ContactUsPageRoutingModule"]
            ],
            declarations: [_contact_us_page__WEBPACK_IMPORTED_MODULE_6__["ContactUsPage"]]
        })
    ], ContactUsPageModule);
    return ContactUsPageModule;
}());



/***/ }),

/***/ "./src/app/contact-us/contact-us.page.scss":
/*!*************************************************!*\
  !*** ./src/app/contact-us/contact-us.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".cssH2 {\n  color: #e2001a;\n  margin: 0 0 15px;\n  font-size: 22px;\n  font-weight: 800;\n  line-height: 1.3;\n}\n\n.cssLabelText {\n  margin-bottom: 5px;\n  color: #666;\n  font-size: 13px;\n}\n\nion-input, ion-textarea {\n  --padding-start: 8px !important;\n  border: 1px solid #999999;\n  border-radius: 3px;\n}\n\n.cssButtonSubmit {\n  font-size: 20px;\n  height: 32px;\n  width: 70px;\n}\n\np {\n  line-height: 22px;\n  margin: 0 0 10px;\n  color: #666;\n  font-size: 13px;\n  line-height: 1.5;\n  letter-spacing: 0.5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9ha2Fhc2hkZXYvUHJvZ3JhbW1pbmcvT2ZmaWNlV29yay9pbmRpYW4tZ3JpbGwvc3JjL2FwcC9jb250YWN0LXVzL2NvbnRhY3QtdXMucGFnZS5zY3NzIiwic3JjL2FwcC9jb250YWN0LXVzL2NvbnRhY3QtdXMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksY0FBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7QUNDSjs7QURFQTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7QUNDSjs7QURFQTtFQUNJLCtCQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtBQ0NKOztBREVBO0VBQ0ksZUFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FDQ0o7O0FERUE7RUFDSSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLHFCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9jb250YWN0LXVzL2NvbnRhY3QtdXMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNzc0gyIHtcbiAgICBjb2xvcjogI2UyMDAxYTtcbiAgICBtYXJnaW46IDAgMCAxNXB4O1xuICAgIGZvbnQtc2l6ZTogMjJweDtcbiAgICBmb250LXdlaWdodDogODAwO1xuICAgIGxpbmUtaGVpZ2h0OiAxLjM7XG59XG5cbi5jc3NMYWJlbFRleHQge1xuICAgIG1hcmdpbi1ib3R0b206IDVweDtcbiAgICBjb2xvcjogIzY2NjtcbiAgICBmb250LXNpemU6IDEzcHg7XG59XG5cbmlvbi1pbnB1dCwgaW9uLXRleHRhcmVhIHtcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDhweCAhaW1wb3J0YW50O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICM5OTk5OTk7XG4gICAgYm9yZGVyLXJhZGl1czogM3B4O1xufVxuXG4uY3NzQnV0dG9uU3VibWl0IHtcbiAgICBmb250LXNpemU6IDIwcHg7XG4gICAgaGVpZ2h0OiAzMnB4O1xuICAgIHdpZHRoOiA3MHB4O1xufVxuXG5wIHtcbiAgICBsaW5lLWhlaWdodDogMjJweDtcbiAgICBtYXJnaW46IDAgMCAxMHB4O1xuICAgIGNvbG9yOiAjNjY2O1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBsaW5lLWhlaWdodDogMS41O1xuICAgIGxldHRlci1zcGFjaW5nOiAwLjVweDtcbn0iLCIuY3NzSDIge1xuICBjb2xvcjogI2UyMDAxYTtcbiAgbWFyZ2luOiAwIDAgMTVweDtcbiAgZm9udC1zaXplOiAyMnB4O1xuICBmb250LXdlaWdodDogODAwO1xuICBsaW5lLWhlaWdodDogMS4zO1xufVxuXG4uY3NzTGFiZWxUZXh0IHtcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xuICBjb2xvcjogIzY2NjtcbiAgZm9udC1zaXplOiAxM3B4O1xufVxuXG5pb24taW5wdXQsIGlvbi10ZXh0YXJlYSB7XG4gIC0tcGFkZGluZy1zdGFydDogOHB4ICFpbXBvcnRhbnQ7XG4gIGJvcmRlcjogMXB4IHNvbGlkICM5OTk5OTk7XG4gIGJvcmRlci1yYWRpdXM6IDNweDtcbn1cblxuLmNzc0J1dHRvblN1Ym1pdCB7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgaGVpZ2h0OiAzMnB4O1xuICB3aWR0aDogNzBweDtcbn1cblxucCB7XG4gIGxpbmUtaGVpZ2h0OiAyMnB4O1xuICBtYXJnaW46IDAgMCAxMHB4O1xuICBjb2xvcjogIzY2NjtcbiAgZm9udC1zaXplOiAxM3B4O1xuICBsaW5lLWhlaWdodDogMS41O1xuICBsZXR0ZXItc3BhY2luZzogMC41cHg7XG59Il19 */");

/***/ }),

/***/ "./src/app/contact-us/contact-us.page.ts":
/*!***********************************************!*\
  !*** ./src/app/contact-us/contact-us.page.ts ***!
  \***********************************************/
/*! exports provided: ContactUsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactUsPage", function() { return ContactUsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_api_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/api-service.service */ "./src/app/services/api-service.service.ts");





var ContactUsPage = /** @class */ (function () {
    function ContactUsPage(loadingCtrl, alertCtrl, api) {
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.api = api;
    }
    ContactUsPage.prototype.ngOnInit = function () {
    };
    ContactUsPage.prototype.onCheckValid = function () {
        return this.contactUsForm.value.comment && this.contactUsForm.value.comment.trim().length === 0;
    };
    ContactUsPage.prototype.onSubmit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading, formBody;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Submitting Form...'
                        })];
                    case 1:
                        loading = _a.sent();
                        loading.present();
                        formBody = {
                            name: this.contactUsForm.value.name,
                            email: this.contactUsForm.value.email,
                            tel: this.contactUsForm.value.telephone,
                            message: this.contactUsForm.value.comment
                        };
                        console.log('Form Data: ', formBody);
                        this.api.contactFn(formBody).then(function (result) {
                            var fetchedData = result;
                            console.log('Response: ', fetchedData);
                            if (fetchedData.status == 200) {
                                _this.presentAlert('Information', fetchedData.message);
                                loading.dismiss();
                            }
                        }).catch(function (err) {
                            console.log(err);
                            _this.presentAlert('Alert', err.error.message);
                            loading.dismiss();
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    /*Alert Service*/
    ContactUsPage.prototype.presentAlert = function (fetchedHDR, fetchedMSG) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: "" + fetchedHDR,
                            message: "" + fetchedMSG,
                            buttons: [
                                {
                                    text: 'Okay',
                                    handler: function () {
                                        _this.contactUsForm.reset();
                                        return;
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        alert.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    ContactUsPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
        { type: _services_api_service_service__WEBPACK_IMPORTED_MODULE_4__["ApiServiceService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('contactUsForm', null),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgForm"])
    ], ContactUsPage.prototype, "contactUsForm", void 0);
    ContactUsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-contact-us',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./contact-us.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/contact-us/contact-us.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./contact-us.page.scss */ "./src/app/contact-us/contact-us.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
            _services_api_service_service__WEBPACK_IMPORTED_MODULE_4__["ApiServiceService"]])
    ], ContactUsPage);
    return ContactUsPage;
}());



/***/ })

}]);
//# sourceMappingURL=contact-us-contact-us-module.js.map