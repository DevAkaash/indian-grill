(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["about-us-about-us-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/about-us/about-us.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/about-us/about-us.page.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button meni=\"m1\">\n        <ion-icon name=\"menu\"></ion-icon>\n      </ion-menu-button>\n    </ion-buttons>\n    <ion-title>About Us</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size-xl=\"6\" offset-xl=\"3\">\n        <div class=\"cssDivAboutUs\">\n          <div>\n            <img\n              src=\"https://www.indian-grill.com/wp-content/uploads/2016/11/IMG_0118.jpg\"\n            />\n          </div>\n          <div class=\"cssDivWhoWeAre\" *ngIf=\"fetchedDATA != null\">\n            <h2 class=\"cssH2\">{{fetchedDATA.title}}</h2>\n            <p class=\"cssPAboutUs\">\n              {{fetchedDATA.about}}\n            </p>\n            <p class=\"cssPFeatured\">\n              {{fetchedDATA.goal}}\n            </p>\n          </div>\n        </div>\n        <br>\n        <div>\n          <img src=\"https://www.indian-grill.com/wp-content/uploads/2016/11/dine-in-area.jpg\" />\n        </div>\n        <div>\n          <h2 class=\"cssBusinessInfo\">BUSINESS INFO</h2>\n          <h3 class=\"cssBusinessDetails\">Business Details</h3>\n          <ion-row>\n            <ion-col size=\"4\">\n              <ion-label class=\"cssLabelInfo\">Cuisines</ion-label>\n            </ion-col>\n            <ion-col size=\"8\">\n              <ion-label class=\"cssLabelInfo\">Buffet, fast food and Indian/Pakistani</ion-label>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col size=\"4\">\n              <ion-label class=\"cssLabelInfo\">Parking</ion-label>\n            </ion-col>\n            <ion-col size=\"8\">\n              <ion-label class=\"cssLabelInfo\">Parking lot parking</ion-label>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col size=\"4\">\n              <ion-label class=\"cssLabelInfo\">Price Range</ion-label>\n            </ion-col>\n            <ion-col size=\"8\">\n              <ion-label class=\"cssLabelInfo\">$</ion-label>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col size=\"4\">\n              <ion-label class=\"cssLabelInfo\">Specialities Serve</ion-label>\n            </ion-col>\n            <ion-col size=\"8\">\n              <ion-label class=\"cssLabelInfo\">Lunch and Dinner</ion-label>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col size=\"4\">\n              <ion-label class=\"cssLabelInfo\">Services</ion-label>\n            </ion-col>\n            <ion-col size=\"8\">\n              <ion-row>\n                <ion-col class=\"cssColList\">\n                  <ion-label class=\"cssLabelInfo\">Walk-ins Welcome</ion-label>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col class=\"cssColList\">\n                  <ion-label class=\"cssLabelInfo\">Good for Groups</ion-label>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col class=\"cssColList\">\n                  <ion-label class=\"cssLabelInfo\">Good for Kids</ion-label>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col class=\"cssColList\">\n                  <ion-label class=\"cssLabelInfo\">Take Outs</ion-label>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col class=\"cssColList\">\n                  <ion-label class=\"cssLabelInfo\">Delivery</ion-label>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col class=\"cssColList\">\n                  <ion-label class=\"cssLabelInfo\">Catering</ion-label>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col class=\"cssColList\">\n                  <ion-label class=\"cssLabelInfo\">Waiter Service</ion-label>\n                </ion-col>\n              </ion-row>\n            </ion-col>\n          </ion-row>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/about-us/about-us-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/about-us/about-us-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: AboutUsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutUsPageRoutingModule", function() { return AboutUsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _about_us_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./about-us.page */ "./src/app/about-us/about-us.page.ts");




var routes = [
    {
        path: '',
        component: _about_us_page__WEBPACK_IMPORTED_MODULE_3__["AboutUsPage"]
    }
];
var AboutUsPageRoutingModule = /** @class */ (function () {
    function AboutUsPageRoutingModule() {
    }
    AboutUsPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], AboutUsPageRoutingModule);
    return AboutUsPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/about-us/about-us.module.ts":
/*!*********************************************!*\
  !*** ./src/app/about-us/about-us.module.ts ***!
  \*********************************************/
/*! exports provided: AboutUsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutUsPageModule", function() { return AboutUsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _about_us_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./about-us-routing.module */ "./src/app/about-us/about-us-routing.module.ts");
/* harmony import */ var _about_us_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./about-us.page */ "./src/app/about-us/about-us.page.ts");







var AboutUsPageModule = /** @class */ (function () {
    function AboutUsPageModule() {
    }
    AboutUsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _about_us_routing_module__WEBPACK_IMPORTED_MODULE_5__["AboutUsPageRoutingModule"]
            ],
            declarations: [_about_us_page__WEBPACK_IMPORTED_MODULE_6__["AboutUsPage"]]
        })
    ], AboutUsPageModule);
    return AboutUsPageModule;
}());



/***/ }),

/***/ "./src/app/about-us/about-us.page.scss":
/*!*********************************************!*\
  !*** ./src/app/about-us/about-us.page.scss ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".cssDivAboutUs {\n  background: #f1f1f1;\n}\n\n.cssDivWhoWeAre {\n  padding: 30px 30px 20px;\n}\n\n.cssH2 {\n  color: #e2001a;\n  margin: 0 0 15px;\n  font-size: 22px;\n  font-weight: 800;\n  line-height: 1.3;\n}\n\np {\n  line-height: 22px;\n  margin: 0 0 10px;\n}\n\n.cssPAboutUs {\n  color: #666;\n  font-size: 13px;\n  letter-spacing: 0.5px;\n}\n\n.cssPFeatured {\n  color: #666;\n  font-style: italic;\n  font-weight: 700;\n  margin-top: 20px;\n  text-transform: uppercase;\n}\n\n.cssBusinessInfo {\n  margin-top: 20px;\n  border-bottom: 1px solid #e5e5e5;\n  color: #e2001a;\n  padding-bottom: 10px;\n  font-size: 22px;\n  font-weight: 800;\n  line-height: 1.3;\n}\n\n.cssBusinessDetails {\n  padding-top: 0px;\n  margin-top: 15px;\n  margin-bottom: 10px;\n  color: #444;\n  font-size: 18px;\n  font-weight: 700;\n  line-height: 1.3;\n}\n\n.cssLabelInfo {\n  color: #666;\n  font-size: 13px;\n  line-height: 1.5;\n  letter-spacing: 0.5px;\n}\n\n.cssColList {\n  padding: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9ha2Fhc2hkZXYvUHJvZ3JhbW1pbmcvT2ZmaWNlV29yay9pbmRpYW4tZ3JpbGwvc3JjL2FwcC9hYm91dC11cy9hYm91dC11cy5wYWdlLnNjc3MiLCJzcmMvYXBwL2Fib3V0LXVzL2Fib3V0LXVzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG1CQUFBO0FDQ0o7O0FERUE7RUFDSSx1QkFBQTtBQ0NKOztBREVBO0VBQ0ksY0FBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7QUNDSjs7QURFQTtFQUNJLGlCQUFBO0VBQ0EsZ0JBQUE7QUNDSjs7QURFQTtFQUNJLFdBQUE7RUFDQSxlQUFBO0VBQ0EscUJBQUE7QUNDSjs7QURFQTtFQUNJLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSx5QkFBQTtBQ0NKOztBREVBO0VBQ0ksZ0JBQUE7RUFDQSxnQ0FBQTtFQUNBLGNBQUE7RUFDQSxvQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0FDQ0o7O0FERUE7RUFDSSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7QUNDSjs7QURHQTtFQUNJLFdBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxxQkFBQTtBQ0FKOztBREdBO0VBQ0ksVUFBQTtBQ0FKIiwiZmlsZSI6InNyYy9hcHAvYWJvdXQtdXMvYWJvdXQtdXMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNzc0RpdkFib3V0VXMge1xuICAgIGJhY2tncm91bmQ6ICNmMWYxZjE7XG59XG5cbi5jc3NEaXZXaG9XZUFyZSB7XG4gICAgcGFkZGluZzogMzBweCAzMHB4IDIwcHg7XG59XG5cbi5jc3NIMiB7XG4gICAgY29sb3I6ICNlMjAwMWE7XG4gICAgbWFyZ2luOiAwIDAgMTVweDtcbiAgICBmb250LXNpemU6IDIycHg7XG4gICAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgICBsaW5lLWhlaWdodDogMS4zO1xufVxuXG5wIHtcbiAgICBsaW5lLWhlaWdodDogMjJweDtcbiAgICBtYXJnaW46IDAgMCAxMHB4O1xufVxuXG4uY3NzUEFib3V0VXMge1xuICAgIGNvbG9yOiAjNjY2O1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMC41cHg7XG59XG5cbi5jc3NQRmVhdHVyZWQge1xuICAgIGNvbG9yOiAjNjY2O1xuICAgIGZvbnQtc3R5bGU6IGl0YWxpYztcbiAgICBmb250LXdlaWdodDogNzAwO1xuICAgIG1hcmdpbi10b3A6IDIwcHg7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cblxuLmNzc0J1c2luZXNzSW5mbyB7XG4gICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2U1ZTVlNTtcbiAgICBjb2xvcjogI2UyMDAxYTtcbiAgICBwYWRkaW5nLWJvdHRvbTogMTBweDtcbiAgICBmb250LXNpemU6IDIycHg7XG4gICAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgICBsaW5lLWhlaWdodDogMS4zO1xufVxuXG4uY3NzQnVzaW5lc3NEZXRhaWxzIHtcbiAgICBwYWRkaW5nLXRvcDogMHB4O1xuICAgIG1hcmdpbi10b3A6IDE1cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgICBjb2xvcjogIzQ0NDtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgICBsaW5lLWhlaWdodDogMS4zO1xuXG59XG5cbi5jc3NMYWJlbEluZm8ge1xuICAgIGNvbG9yOiAjNjY2O1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBsaW5lLWhlaWdodDogMS41O1xuICAgIGxldHRlci1zcGFjaW5nOiAwLjVweDtcbn1cblxuLmNzc0NvbExpc3Qge1xuICAgIHBhZGRpbmc6IDA7XG59IiwiLmNzc0RpdkFib3V0VXMge1xuICBiYWNrZ3JvdW5kOiAjZjFmMWYxO1xufVxuXG4uY3NzRGl2V2hvV2VBcmUge1xuICBwYWRkaW5nOiAzMHB4IDMwcHggMjBweDtcbn1cblxuLmNzc0gyIHtcbiAgY29sb3I6ICNlMjAwMWE7XG4gIG1hcmdpbjogMCAwIDE1cHg7XG4gIGZvbnQtc2l6ZTogMjJweDtcbiAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgbGluZS1oZWlnaHQ6IDEuMztcbn1cblxucCB7XG4gIGxpbmUtaGVpZ2h0OiAyMnB4O1xuICBtYXJnaW46IDAgMCAxMHB4O1xufVxuXG4uY3NzUEFib3V0VXMge1xuICBjb2xvcjogIzY2NjtcbiAgZm9udC1zaXplOiAxM3B4O1xuICBsZXR0ZXItc3BhY2luZzogMC41cHg7XG59XG5cbi5jc3NQRmVhdHVyZWQge1xuICBjb2xvcjogIzY2NjtcbiAgZm9udC1zdHlsZTogaXRhbGljO1xuICBmb250LXdlaWdodDogNzAwO1xuICBtYXJnaW4tdG9wOiAyMHB4O1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xufVxuXG4uY3NzQnVzaW5lc3NJbmZvIHtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNlNWU1ZTU7XG4gIGNvbG9yOiAjZTIwMDFhO1xuICBwYWRkaW5nLWJvdHRvbTogMTBweDtcbiAgZm9udC1zaXplOiAyMnB4O1xuICBmb250LXdlaWdodDogODAwO1xuICBsaW5lLWhlaWdodDogMS4zO1xufVxuXG4uY3NzQnVzaW5lc3NEZXRhaWxzIHtcbiAgcGFkZGluZy10b3A6IDBweDtcbiAgbWFyZ2luLXRvcDogMTVweDtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgY29sb3I6ICM0NDQ7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgbGluZS1oZWlnaHQ6IDEuMztcbn1cblxuLmNzc0xhYmVsSW5mbyB7XG4gIGNvbG9yOiAjNjY2O1xuICBmb250LXNpemU6IDEzcHg7XG4gIGxpbmUtaGVpZ2h0OiAxLjU7XG4gIGxldHRlci1zcGFjaW5nOiAwLjVweDtcbn1cblxuLmNzc0NvbExpc3Qge1xuICBwYWRkaW5nOiAwO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/about-us/about-us.page.ts":
/*!*******************************************!*\
  !*** ./src/app/about-us/about-us.page.ts ***!
  \*******************************************/
/*! exports provided: AboutUsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutUsPage", function() { return AboutUsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_api_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/api-service.service */ "./src/app/services/api-service.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");




var AboutUsPage = /** @class */ (function () {
    function AboutUsPage(api, loadingCtrl) {
        this.api = api;
        this.loadingCtrl = loadingCtrl;
    }
    AboutUsPage.prototype.ngOnInit = function () {
        this.getAbout();
    };
    AboutUsPage.prototype.getAbout = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({})];
                    case 1:
                        loading = _a.sent();
                        loading.present();
                        this.api.aboutUSfn().then(function (result) {
                            var aboutUS = result;
                            console.log('About US: ', aboutUS);
                            if (aboutUS.status == 200) {
                                _this.fetchedDATA = aboutUS;
                                loading.dismiss();
                            }
                        }).catch(function (err) {
                            console.log('err', err);
                            loading.dismiss();
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    AboutUsPage.ctorParameters = function () { return [
        { type: _services_api_service_service__WEBPACK_IMPORTED_MODULE_2__["ApiServiceService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] }
    ]; };
    AboutUsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-about-us',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./about-us.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/about-us/about-us.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./about-us.page.scss */ "./src/app/about-us/about-us.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_api_service_service__WEBPACK_IMPORTED_MODULE_2__["ApiServiceService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]])
    ], AboutUsPage);
    return AboutUsPage;
}());



/***/ })

}]);
//# sourceMappingURL=about-us-about-us-module.js.map