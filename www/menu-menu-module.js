(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["menu-menu-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/menu/menu.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/menu/menu.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button meni=\"m1\">\n        <ion-icon name=\"menu\"></ion-icon>\n      </ion-menu-button>\n    </ion-buttons>\n    <ion-title>Order Now</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size-xl=\"6\" offset-xl=\"3\">\n        <div>\n          <ion-slides pager=\"true\" [options]=\"slideOpts\">\n            <ion-slide>\n              <img\n                src=\"https://www.indian-grill.com/wp-content/uploads/2016/11/IMG_0118.jpg\"\n              />\n            </ion-slide>\n            <ion-slide>\n              <img\n                src=\"https://www.indian-grill.com/wp-content/uploads/2016/11/dine-in-area.jpg\"\n              />\n            </ion-slide>\n            <ion-slide>\n              <img\n                src=\"https://www.indian-grill.com/wp-content/uploads/2016/11/IMG_0118.jpg\"\n              />\n            </ion-slide>\n          </ion-slides>\n        </div>\n        <div>\n          <p>\n            For any type of special event, our professionals staff serves best\n            in class facilities and services. Our goal is to make your moment\n            memorable. Indian Grill helps you to bring your dream plan into\n            reality. We make sure to serve you with world-class event planning\n            services.So, if you want to make your dream event memorable, Indian\n            Grill is the best option to achieve that with utmost elegance.\n          </p>\n        </div>\n        <div class=\"accordion\">\n          <!-- <ion-list>\n            <ion-item lines=\"none\" button class=\"cssMenuName\" detail=\"false\" *ngFor=\"let m of menu\" (click)=\"toggleAccordion(m)\">\n              <ion-icon name=\"add\" color=\"light\" slot=\"start\" *ngIf=\"!m.isMenuOpen\"></ion-icon>\n              <ion-icon name=\"remove\" color=\"light\" slot=\"start\" *ngIf=\"m.isMenuOpen\"></ion-icon>\n              <ion-label>{{m.name}}</ion-label>\n            </ion-item>\n          </ion-list> -->\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/menu/menu-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/menu/menu-routing.module.ts ***!
  \*********************************************/
/*! exports provided: MenuPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuPageRoutingModule", function() { return MenuPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _menu_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./menu.page */ "./src/app/menu/menu.page.ts");




var routes = [
    {
        path: '',
        component: _menu_page__WEBPACK_IMPORTED_MODULE_3__["MenuPage"]
    }
];
var MenuPageRoutingModule = /** @class */ (function () {
    function MenuPageRoutingModule() {
    }
    MenuPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], MenuPageRoutingModule);
    return MenuPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/menu/menu.module.ts":
/*!*************************************!*\
  !*** ./src/app/menu/menu.module.ts ***!
  \*************************************/
/*! exports provided: MenuPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuPageModule", function() { return MenuPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _menu_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./menu-routing.module */ "./src/app/menu/menu-routing.module.ts");
/* harmony import */ var _menu_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./menu.page */ "./src/app/menu/menu.page.ts");







var MenuPageModule = /** @class */ (function () {
    function MenuPageModule() {
    }
    MenuPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _menu_routing_module__WEBPACK_IMPORTED_MODULE_5__["MenuPageRoutingModule"]
            ],
            declarations: [_menu_page__WEBPACK_IMPORTED_MODULE_6__["MenuPage"]],
        })
    ], MenuPageModule);
    return MenuPageModule;
}());



/***/ }),

/***/ "./src/app/menu/menu.page.scss":
/*!*************************************!*\
  !*** ./src/app/menu/menu.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("img {\n  height: 375px;\n  width: 100%;\n}\n\np {\n  line-height: 22px;\n  margin: 0 0 10px;\n  color: #666;\n  font-size: 15px;\n  line-height: 1.5;\n}\n\n.cssMenuName {\n  --background: #e2001a;\n  color: #fff;\n  margin: 5px;\n  border-radius: 3px;\n  border: 1px solid #e2001a86;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9ha2Fhc2hkZXYvUHJvZ3JhbW1pbmcvT2ZmaWNlV29yay9pbmRpYW4tZ3JpbGwvc3JjL2FwcC9tZW51L21lbnUucGFnZS5zY3NzIiwic3JjL2FwcC9tZW51L21lbnUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksYUFBQTtFQUNBLFdBQUE7QUNDSjs7QURFQTtFQUNJLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FDQ0o7O0FER0E7RUFDSSxxQkFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSwyQkFBQTtBQ0FKIiwiZmlsZSI6InNyYy9hcHAvbWVudS9tZW51LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImltZyB7XG4gICAgaGVpZ2h0OiAzNzVweDtcbiAgICB3aWR0aDogMTAwJTtcbn1cblxucCB7XG4gICAgbGluZS1oZWlnaHQ6IDIycHg7XG4gICAgbWFyZ2luOiAwIDAgMTBweDtcbiAgICBjb2xvcjogIzY2NjtcbiAgICBmb250LXNpemU6IDE1cHg7XG4gICAgbGluZS1oZWlnaHQ6IDEuNTtcbiAgICAvLyBsZXR0ZXItc3BhY2luZzogMC41cHg7XG59XG5cbi5jc3NNZW51TmFtZSB7XG4gICAgLS1iYWNrZ3JvdW5kOiAjZTIwMDFhO1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIG1hcmdpbjogNXB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDNweDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZTIwMDFhODY7XG59IiwiaW1nIHtcbiAgaGVpZ2h0OiAzNzVweDtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbnAge1xuICBsaW5lLWhlaWdodDogMjJweDtcbiAgbWFyZ2luOiAwIDAgMTBweDtcbiAgY29sb3I6ICM2NjY7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgbGluZS1oZWlnaHQ6IDEuNTtcbn1cblxuLmNzc01lbnVOYW1lIHtcbiAgLS1iYWNrZ3JvdW5kOiAjZTIwMDFhO1xuICBjb2xvcjogI2ZmZjtcbiAgbWFyZ2luOiA1cHg7XG4gIGJvcmRlci1yYWRpdXM6IDNweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2UyMDAxYTg2O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/menu/menu.page.ts":
/*!***********************************!*\
  !*** ./src/app/menu/menu.page.ts ***!
  \***********************************/
/*! exports provided: MenuPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuPage", function() { return MenuPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var MenuPage = /** @class */ (function () {
    function MenuPage() {
        this.slideOpts = {
            initialSlide: 0,
            speed: 400
        };
        this.menu = [
            {
                name: 'Veg Appetizers',
                menuItem: ['Veg Pakoda', 'Batata Vada', 'Allu Tikki Chaat', 'Samosa Chaat', 'Bhel'],
                isMenuOpen: true
            },
            {
                name: 'Non Veg Appetizers',
                menuItem: ['Chicken Tikka', 'Chicken Seekh Kebab', 'Chicken Malai Kebab', 'Tandoori Chicken', 'Chicken Mannchurian'],
                isMenuOpen: false
            },
            {
                name: 'Veg Entree',
                menuItem: ['Matar Paneer', 'Malai Kofta', 'Saag Paneer', 'Paneer Tikka Masala', 'Paneer Makhni'],
                isMenuOpen: false
            },
            {
                name: 'Non Veg Entree',
                menuItem: ['Chicken Tikka Masala', 'Chicken Makhani', 'Chilly Chicken Curry', 'Chicken Korma', 'Karahi Chicken'],
                isMenuOpen: false
            },
            {
                name: 'Desserts',
                menuItem: ['Fruit Custard', 'Kheer', 'Gulab Jamun', 'Rasmalai', 'Ice-Cream'],
                isMenuOpen: false
            },
        ];
    }
    MenuPage.prototype.toggleAccordion = function (menu) {
        console.log(menu);
        this.menu.map(function (listItem) {
            if (menu == listItem) {
                listItem.isMenuOpen = !listItem.isMenuOpen;
            }
            else {
                listItem.isMenuOpen = false;
            }
            return listItem;
        });
    };
    MenuPage.prototype.ngOnInit = function () {
    };
    MenuPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-menu',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./menu.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/menu/menu.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./menu.page.scss */ "./src/app/menu/menu.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], MenuPage);
    return MenuPage;
}());



/***/ })

}]);
//# sourceMappingURL=menu-menu-module.js.map