(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./node_modules/@ionic-native/in-app-browser/ngx/index.js":
/*!****************************************************************!*\
  !*** ./node_modules/@ionic-native/in-app-browser/ngx/index.js ***!
  \****************************************************************/
/*! exports provided: InAppBrowserObject, InAppBrowser */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InAppBrowserObject", function() { return InAppBrowserObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InAppBrowser", function() { return InAppBrowser; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/core */ "./node_modules/@ionic-native/core/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");




var InAppBrowserObject = /** @class */ (function () {
    /**
     * Opens a URL in a new InAppBrowser instance, the current browser instance, or the system browser.
     * @param {string} url     The URL to load.
     * @param {string} [target="self"]  The target in which to load the URL, an optional parameter that defaults to _self.
     *                 _self: Opens in the WebView if the URL is in the white list, otherwise it opens in the InAppBrowser.
     *                 _blank: Opens in the InAppBrowser.
     *                 _system: Opens in the system's web browser.
     * @param {string | InAppBrowserOptions} [options] Options for the InAppBrowser. Optional, defaulting to: location=yes.
     *                 The options string must not contain any blank space, and each feature's
     *                 name/value pairs must be separated by a comma. Feature names are case insensitive.
     */
    function InAppBrowserObject(url, target, options) {
        try {
            if (options && typeof options !== 'string') {
                options = Object.keys(options)
                    .map(function (key) { return key + "=" + options[key]; })
                    .join(',');
            }
            this._objectInstance = cordova.InAppBrowser.open(url, target, options);
        }
        catch (e) {
            if (typeof window !== 'undefined') {
                window.open(url, target);
            }
            console.warn('Native: InAppBrowser is not installed or you are running on a browser. Falling back to window.open.');
        }
    }
    InAppBrowserObject.prototype._loadAfterBeforeload = function (strUrl) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordovaInstance"])(this, "_loadAfterBeforeload", { "sync": true }, arguments); };
    InAppBrowserObject.prototype.show = function () { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordovaInstance"])(this, "show", { "sync": true }, arguments); };
    InAppBrowserObject.prototype.close = function () { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordovaInstance"])(this, "close", { "sync": true }, arguments); };
    InAppBrowserObject.prototype.hide = function () { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordovaInstance"])(this, "hide", { "sync": true }, arguments); };
    InAppBrowserObject.prototype.executeScript = function (script) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordovaInstance"])(this, "executeScript", {}, arguments); };
    InAppBrowserObject.prototype.insertCSS = function (css) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordovaInstance"])(this, "insertCSS", {}, arguments); };
    InAppBrowserObject.prototype.on = function (event) {
        var _this = this;
        return (function () {
            if (Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["instanceAvailability"])(_this) === true) {
                return new rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"](function (observer) {
                    _this._objectInstance.addEventListener(event, observer.next.bind(observer));
                    return function () { return _this._objectInstance.removeEventListener(event, observer.next.bind(observer)); };
                });
            }
        })();
    };
    InAppBrowserObject.prototype.on = function (event) {
        var _this = this;
        return (function () {
            if (Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["instanceAvailability"])(_this) === true) {
                return new rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"](function (observer) {
                    _this._objectInstance.addEventListener(event, observer.next.bind(observer));
                    return function () { return _this._objectInstance.removeEventListener(event, observer.next.bind(observer)); };
                });
            }
        })();
    };
    return InAppBrowserObject;
}());

var InAppBrowser = /** @class */ (function (_super) {
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"])(InAppBrowser, _super);
    function InAppBrowser() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * Opens a URL in a new InAppBrowser instance, the current browser instance, or the system browser.
     * @param  url {string}     The URL to load.
     * @param  target {string}  The target in which to load the URL, an optional parameter that defaults to _self.
     * @param  options {string} Options for the InAppBrowser. Optional, defaulting to: location=yes.
     *                 The options string must not contain any blank space, and each feature's
     *                 name/value pairs must be separated by a comma. Feature names are case insensitive.
     * @returns {InAppBrowserObject}
     */
    InAppBrowser.prototype.create = function (url, target, options) {
        return new InAppBrowserObject(url, target, options);
    };
    InAppBrowser.pluginName = "InAppBrowser";
    InAppBrowser.plugin = "cordova-plugin-inappbrowser";
    InAppBrowser.pluginRef = "cordova.InAppBrowser";
    InAppBrowser.repo = "https://github.com/apache/cordova-plugin-inappbrowser";
    InAppBrowser.platforms = ["AmazonFire OS", "Android", "Browser", "iOS", "macOS", "Windows"];
    InAppBrowser = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], InAppBrowser);
    return InAppBrowser;
}(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["IonicNativePlugin"]));

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9zcmMvQGlvbmljLW5hdGl2ZS9wbHVnaW5zL2luLWFwcC1icm93c2VyL25neC9pbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLDREQUE2RCxNQUFNLG9CQUFvQixDQUFDO0FBQy9GLE9BQU8sRUFBRSxVQUFVLEVBQVksTUFBTSxNQUFNLENBQUM7O0lBa0oxQzs7Ozs7Ozs7OztPQVVHO0lBQ0gsNEJBQVksR0FBVyxFQUFFLE1BQWUsRUFBRSxPQUFzQztRQUM5RSxJQUFJO1lBQ0YsSUFBSSxPQUFPLElBQUksT0FBTyxPQUFPLEtBQUssUUFBUSxFQUFFO2dCQUMxQyxPQUFPLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUM7cUJBQzNCLEdBQUcsQ0FBQyxVQUFDLEdBQVcsSUFBSyxPQUFHLEdBQUcsU0FBSyxPQUErQixDQUFDLEdBQUcsQ0FBRyxFQUFqRCxDQUFpRCxDQUFDO3FCQUN2RSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7YUFDZDtZQUVELElBQUksQ0FBQyxlQUFlLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLE1BQU0sRUFBRSxPQUFPLENBQUMsQ0FBQztTQUN4RTtRQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ1YsSUFBSSxPQUFPLE1BQU0sS0FBSyxXQUFXLEVBQUU7Z0JBQ2pDLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLE1BQU0sQ0FBQyxDQUFDO2FBQzFCO1lBQ0QsT0FBTyxDQUFDLElBQUksQ0FDVixxR0FBcUcsQ0FDdEcsQ0FBQztTQUNIO0lBQ0gsQ0FBQztJQU9ELGlEQUFvQixhQUFDLE1BQWM7SUFPbkMsaUNBQUk7SUFNSixrQ0FBSztJQU9MLGlDQUFJO0lBUUosMENBQWEsYUFBQyxNQUF3QztJQVV0RCxzQ0FBUyxhQUFDLEdBQXFDO0lBVS9DLCtCQUFFLGFBQUMsS0FBNEI7OztzREFBaUM7Z0JBQzlELE9BQU8sSUFBSSxVQUFVLENBQW9CLFVBQUMsUUFBcUM7b0JBQzdFLEtBQUksQ0FBQyxlQUFlLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxFQUFFLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7b0JBQzNFLE9BQU8sY0FBTSxPQUFBLEtBQUksQ0FBQyxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQTdFLENBQTZFLENBQUM7Z0JBQzdGLENBQUMsQ0FBQyxDQUFDO2FBQ0o7OztJQVFELCtCQUFFLGFBQUMsS0FBYTs7O3NEQUFpQztnQkFDL0MsT0FBTyxJQUFJLFVBQVUsQ0FBb0IsVUFBQyxRQUFxQztvQkFDN0UsS0FBSSxDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUUsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztvQkFDM0UsT0FBTyxjQUFNLE9BQUEsS0FBSSxDQUFDLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBN0UsQ0FBNkUsQ0FBQztnQkFDN0YsQ0FBQyxDQUFDLENBQUM7YUFDSjs7OzZCQXpQSDs7OztJQW1Ta0MsZ0NBQWlCOzs7O0lBQ2pEOzs7Ozs7OztPQVFHO0lBQ0gsNkJBQU0sR0FBTixVQUFPLEdBQVcsRUFBRSxNQUFlLEVBQUUsT0FBc0M7UUFDekUsT0FBTyxJQUFJLGtCQUFrQixDQUFDLEdBQUcsRUFBRSxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFDdEQsQ0FBQzs7Ozs7O0lBWlUsWUFBWTtRQUR4QixVQUFVLEVBQUU7T0FDQSxZQUFZO3VCQW5TekI7RUFtU2tDLGlCQUFpQjtTQUF0QyxZQUFZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29yZG92YUluc3RhbmNlLCBJbnN0YW5jZUNoZWNrLCBJb25pY05hdGl2ZVBsdWdpbiwgUGx1Z2luIH0gZnJvbSAnQGlvbmljLW5hdGl2ZS9jb3JlJztcbmltcG9ydCB7IE9ic2VydmFibGUsIE9ic2VydmVyIH0gZnJvbSAncnhqcyc7XG5cbmRlY2xhcmUgY29uc3QgY29yZG92YTogQ29yZG92YSAmIHsgSW5BcHBCcm93c2VyOiBhbnkgfTtcblxuZXhwb3J0IGludGVyZmFjZSBJbkFwcEJyb3dzZXJPcHRpb25zIHtcbiAgLyoqXG4gICAqIChpT1MgT25seSkgU2V0IHRvIHllcyBvciBubyB0byBhbGxvdyBpbi1saW5lIEhUTUw1IG1lZGlhIHBsYXliYWNrLCBkaXNwbGF5aW5nIHdpdGhpbiB0aGUgYnJvd3NlciB3aW5kb3cgcmF0aGVyIHRoYW4gYSBkZXZpY2Utc3BlY2lmaWMgcGxheWJhY2sgaW50ZXJmYWNlLlxuICAgKiBUaGUgSFRNTCdzIHZpZGVvIGVsZW1lbnQgbXVzdCBhbHNvIGluY2x1ZGUgdGhlIHdlYmtpdC1wbGF5c2lubGluZSBhdHRyaWJ1dGUgKGRlZmF1bHRzIHRvIG5vKVxuICAgKi9cbiAgYWxsb3dJbmxpbmVNZWRpYVBsYXliYWNrPzogJ3llcycgfCAnbm8nO1xuICAvKipcbiAgICogc2V0IHRvIGVuYWJsZSB0aGUgYmVmb3JlbG9hZCBldmVudCB0byBtb2RpZnkgd2hpY2ggcGFnZXMgYXJlIGFjdHVhbGx5IGxvYWRlZCBpbiB0aGUgYnJvd3Nlci4gQWNjZXB0ZWQgdmFsdWVzIGFyZSBnZXQgdG9cbiAgICogaW50ZXJjZXB0IG9ubHkgR0VUIHJlcXVlc3RzLCBwb3N0IHRvIGludGVyY2VwdCBvbiBQT1NUIHJlcXVlc3RzIG9yIHllcyB0byBpbnRlcmNlcHQgYm90aCBHRVQgJiBQT1NUIHJlcXVlc3RzLlxuICAgKiBOb3RlIHRoYXQgUE9TVCByZXF1ZXN0cyBhcmUgbm90IGN1cnJlbnRseSBzdXBwb3J0ZWQgYW5kIHdpbGwgYmUgaWdub3JlZCAoaWYgeW91IHNldCBiZWZvcmVsb2FkPXBvc3QgaXQgd2lsbCByYWlzZSBhbiBlcnJvcikuXG4gICAqL1xuICBiZWZvcmVsb2FkPzogJ3llcycgfCAnZ2V0JyB8ICdwb3N0JztcbiAgLyoqIFNldCB0byB5ZXMgdG8gaGF2ZSB0aGUgYnJvd3NlcidzIGNvb2tpZSBjYWNoZSBjbGVhcmVkIGJlZm9yZSB0aGUgbmV3IHdpbmRvdyBpcyBvcGVuZWQuICovXG4gIGNsZWFyY2FjaGU/OiAneWVzJyB8ICdubyc7XG4gIC8qKiAgc2V0IHRvIHllcyB0byBoYXZlIHRoZSBicm93c2VyJ3MgZW50aXJlIGxvY2FsIHN0b3JhZ2UgY2xlYXJlZCAoY29va2llcywgSFRNTDUgbG9jYWwgc3RvcmFnZSwgSW5kZXhlZERCLCBldGMuKSBiZWZvcmUgdGhlIG5ldyB3aW5kb3cgaXMgb3BlbmVkICovXG4gIGNsZWFyZGF0YT86ICd5ZXMnIHwgJ25vJztcbiAgLyoqXG4gICAqIFNldCB0byB5ZXMgdG8gaGF2ZSB0aGUgc2Vzc2lvbiBjb29raWUgY2FjaGUgY2xlYXJlZCBiZWZvcmUgdGhlIG5ldyB3aW5kb3cgaXMgb3BlbmVkLlxuICAgKiBGb3IgV0tXZWJWaWV3LCByZXF1aXJlcyBpT1MgMTErIG9uIHRhcmdldCBkZXZpY2UuXG4gICAqL1xuICBjbGVhcnNlc3Npb25jYWNoZT86ICd5ZXMnIHwgJ25vJztcbiAgLyoqXG4gICAqIChBbmRyb2lkKSBTZXQgdG8gYSBzdHJpbmcgdG8gdXNlIGFzIHRoZSBjbG9zZSBidXR0b24ncyBjYXB0aW9uIGluc3RlYWQgb2YgYSBYLiBOb3RlIHRoYXQgeW91IG5lZWQgdG8gbG9jYWxpemUgdGhpcyB2YWx1ZSB5b3Vyc2VsZi5cbiAgICogKGlPUykgU2V0IHRvIGEgc3RyaW5nIHRvIHVzZSBhcyB0aGUgRG9uZSBidXR0b24ncyBjYXB0aW9uLiBOb3RlIHRoYXQgeW91IG5lZWQgdG8gbG9jYWxpemUgdGhpcyB2YWx1ZSB5b3Vyc2VsZi5cbiAgICovXG4gIGNsb3NlYnV0dG9uY2FwdGlvbj86IHN0cmluZztcbiAgLyoqXG4gICAqIChBbmRyb2lkKSBTZXQgdG8gYSB2YWxpZCBoZXggY29sb3Igc3RyaW5nLCBmb3IgZXhhbXBsZTogIzAwZmYwMCwgYW5kIGl0IHdpbGwgY2hhbmdlIHRoZSBjbG9zZSBidXR0b24gY29sb3IgZnJvbSBkZWZhdWx0LCByZWdhcmRsZXNzIG9mIGJlaW5nIGEgdGV4dCBvciBkZWZhdWx0IFguIE9ubHkgaGFzIGVmZmVjdCBpZiB1c2VyIGhhcyBsb2NhdGlvbiBzZXQgdG8geWVzLlxuICAgKiAoaU9TKSBTZXQgYXMgYSB2YWxpZCBoZXggY29sb3Igc3RyaW5nLCBmb3IgZXhhbXBsZTogIzAwZmYwMCwgdG8gY2hhbmdlIGZyb20gdGhlIGRlZmF1bHQgRG9uZSBidXR0b24ncyBjb2xvci4gT25seSBhcHBsaWNhYmxlIGlmIHRvb2xiYXIgaXMgbm90IGRpc2FibGVkLlxuICAgKi9cbiAgY2xvc2VidXR0b25jb2xvcj86IHN0cmluZztcbiAgLyoqIChpT1MgT25seSkgU2V0IHRvIHllcyBvciBubyAoZGVmYXVsdCBpcyBubykuIFR1cm5zIG9uL29mZiB0aGUgVUlXZWJWaWV3Qm91bmNlIHByb3BlcnR5LiAqL1xuICBkaXNhbGxvd292ZXJzY3JvbGw/OiAneWVzJyB8ICdubyc7XG4gIC8qKiAoaU9TIE9ubHkpICBTZXQgdG8geWVzIG9yIG5vIHRvIHByZXZlbnQgdmlld3BvcnQgc2NhbGluZyB0aHJvdWdoIGEgbWV0YSB0YWcgKGRlZmF1bHRzIHRvIG5vKS4gKi9cbiAgZW5hYmxlVmlld3BvcnRTY2FsZT86ICd5ZXMnIHwgJ25vJztcbiAgLyoqIChBbmRyb2lkIE9ubHkpIFNldCB0byB5ZXMgdG8gc2hvdyBhIGNsb3NlIGJ1dHRvbiBpbiB0aGUgZm9vdGVyIHNpbWlsYXIgdG8gdGhlIGlPUyBEb25lIGJ1dHRvbi4gVGhlIGNsb3NlIGJ1dHRvbiB3aWxsIGFwcGVhciB0aGUgc2FtZSBhcyBmb3IgdGhlIGhlYWRlciBoZW5jZSB1c2UgY2xvc2VidXR0b25jYXB0aW9uIGFuZCBjbG9zZWJ1dHRvbmNvbG9yIHRvIHNldCBpdHMgcHJvcGVydGllcyAqL1xuICBmb290ZXI/OiAneWVzJyB8ICdubyc7XG4gIC8qKiAoQW5kcm9pZCBPbmx5KSBTZXQgdG8gYSB2YWxpZCBoZXggY29sb3Igc3RyaW5nLCBmb3IgZXhhbXBsZSAjMDBmZjAwIG9yICNDQzAwZmYwMCAoI2FhcnJnZ2JiKSwgYW5kIGl0IHdpbGwgY2hhbmdlIHRoZSBmb290ZXIgY29sb3IgZnJvbSBkZWZhdWx0LiBPbmx5IGhhcyBlZmZlY3QgaWYgdXNlciBoYXMgZm9vdGVyIHNldCB0byB5ZXMgKi9cbiAgZm9vdGVyY29sb3I/OiBzdHJpbmc7XG4gIC8qKlxuICAgKiAoV2luZG93cyBvbmx5KSBTZXQgdG8geWVzIHRvIGNyZWF0ZSB0aGUgYnJvd3NlciBjb250cm9sIHdpdGhvdXQgYSBib3JkZXIgYXJvdW5kIGl0LlxuICAgKiBQbGVhc2Ugbm90ZSB0aGF0IGlmIGxvY2F0aW9uPW5vIGlzIGFsc28gc3BlY2lmaWVkLCB0aGVyZSB3aWxsIGJlIG5vIGNvbnRyb2wgcHJlc2VudGVkIHRvIHVzZXIgdG8gY2xvc2UgSUFCIHdpbmRvdy5cbiAgICovXG4gIGZ1bGxzY3JlZW4/OiAneWVzJyB8ICdubyc7XG4gIC8qKlxuICAgKiAoQW5kcm9pZCAmIFdpbmRvd3MgT25seSkgU2V0IHRvIHllcyB0byB1c2UgdGhlIGhhcmR3YXJlIGJhY2sgYnV0dG9uIHRvIG5hdmlnYXRlIGJhY2t3YXJkcyB0aHJvdWdoIHRoZSBJbkFwcEJyb3dzZXIncyBoaXN0b3J5LlxuICAgKiBJZiB0aGVyZSBpcyBubyBwcmV2aW91cyBwYWdlLCB0aGUgSW5BcHBCcm93c2VyIHdpbGwgY2xvc2UuIFRoZSBkZWZhdWx0IHZhbHVlIGlzIHllcywgc28geW91IG11c3Qgc2V0IGl0IHRvIG5vIGlmIHlvdSB3YW50IHRoZSBiYWNrIGJ1dHRvbiB0byBzaW1wbHkgY2xvc2UgdGhlIEluQXBwQnJvd3Nlci5cbiAgICovXG4gIGhhcmR3YXJlYmFjaz86ICd5ZXMnIHwgJ25vJztcbiAgLyoqXG4gICAqIFNldCB0byB5ZXMgdG8gY3JlYXRlIHRoZSBicm93c2VyIGFuZCBsb2FkIHRoZSBwYWdlLCBidXQgbm90IHNob3cgaXQuIFRoZSBsb2Fkc3RvcCBldmVudCBmaXJlcyB3aGVuIGxvYWRpbmcgaXMgY29tcGxldGUuXG4gICAqIE9taXQgb3Igc2V0IHRvIG5vIChkZWZhdWx0KSB0byBoYXZlIHRoZSBicm93c2VyIG9wZW4gYW5kIGxvYWQgbm9ybWFsbHkuXG4gICAqL1xuICBoaWRkZW4/OiAneWVzJyB8ICdubyc7XG4gIC8qKlxuICAgKiAoQW5kcm9pZCkgU2V0IHRvIHllcyB0byBoaWRlIHRoZSBuYXZpZ2F0aW9uIGJ1dHRvbnMgb24gdGhlIGxvY2F0aW9uIHRvb2xiYXIsIG9ubHkgaGFzIGVmZmVjdCBpZiB1c2VyIGhhcyBsb2NhdGlvbiBzZXQgdG8geWVzLiBUaGUgZGVmYXVsdCB2YWx1ZSBpcyBuby5cbiAgICogKGlPUykgU2V0IHRvIHllcyBvciBubyB0byB0dXJuIHRoZSB0b29sYmFyIG5hdmlnYXRpb24gYnV0dG9ucyBvbiBvciBvZmYgKGRlZmF1bHRzIHRvIG5vKS4gT25seSBhcHBsaWNhYmxlIGlmIHRvb2xiYXIgaXMgbm90IGRpc2FibGVkLlxuICAgKi9cbiAgaGlkZW5hdmlnYXRpb25idXR0b25zPzogJ3llcycgfCAnbm8nO1xuICAvKipcbiAgICogIChpT1MgT25seSkgU2V0IHRvIHllcyBvciBubyB0byBjaGFuZ2UgdGhlIHZpc2liaWxpdHkgb2YgdGhlIGxvYWRpbmcgaW5kaWNhdG9yIChkZWZhdWx0cyB0byBubykuXG4gICAqL1xuICBoaWRlc3Bpbm5lcj86ICd5ZXMnIHwgJ25vJztcbiAgLyoqIChBbmRyb2lkKSBTZXQgdG8geWVzIHRvIGhpZGUgdGhlIHVybCBiYXIgb24gdGhlIGxvY2F0aW9uIHRvb2xiYXIsIG9ubHkgaGFzIGVmZmVjdCBpZiB1c2VyIGhhcyBsb2NhdGlvbiBzZXQgdG8geWVzLiBUaGUgZGVmYXVsdCB2YWx1ZSBpcyBuby4gKi9cbiAgaGlkZXVybGJhcj86ICd5ZXMnIHwgJ25vJztcbiAgLyoqIChpT1MgT25seSkgU2V0IHRvIHllcyBvciBubyB0byBvcGVuIHRoZSBrZXlib2FyZCB3aGVuIGZvcm0gZWxlbWVudHMgcmVjZWl2ZSBmb2N1cyB2aWEgSmF2YVNjcmlwdCdzIGZvY3VzKCkgY2FsbCAoZGVmYXVsdHMgdG8geWVzKS4gKi9cbiAga2V5Ym9hcmREaXNwbGF5UmVxdWlyZXNVc2VyQWN0aW9uPzogJ3llcycgfCAnbm8nO1xuICAvKipcbiAgICogKEFuZHJvaWQpIFNldCB0byB5ZXMgdG8gc3dhcCBwb3NpdGlvbnMgb2YgdGhlIG5hdmlnYXRpb24gYnV0dG9ucyBhbmQgdGhlIGNsb3NlIGJ1dHRvbi4gU3BlY2lmaWNhbGx5LCBuYXZpZ2F0aW9uIGJ1dHRvbnMgZ28gdG8gdGhlIGxlZnQgYW5kIGNsb3NlIGJ1dHRvbiB0byB0aGUgcmlnaHQuXG4gICAqIChpT1MpIFNldCB0byB5ZXMgdG8gc3dhcCBwb3NpdGlvbnMgb2YgdGhlIG5hdmlnYXRpb24gYnV0dG9ucyBhbmQgdGhlIGNsb3NlIGJ1dHRvbi4gU3BlY2lmaWNhbGx5LCBjbG9zZSBidXR0b24gZ29lcyB0byB0aGUgcmlnaHQgYW5kIG5hdmlnYXRpb24gYnV0dG9ucyB0byB0aGUgbGVmdC5cbiAgICovXG4gIGxlZnR0b3JpZ2h0PzogJ3llcycgfCAnbm8nO1xuICAvKiogU2V0IHRvIHllcyBvciBubyB0byB0dXJuIHRoZSBJbkFwcEJyb3dzZXIncyBsb2NhdGlvbiBiYXIgb24gb3Igb2ZmLiAqL1xuICBsb2NhdGlvbj86ICd5ZXMnIHwgJ25vJztcbiAgLyoqXG4gICAqICBTZXQgdG8geWVzIHRvIHByZXZlbnQgSFRNTDUgYXVkaW8gb3IgdmlkZW8gZnJvbSBhdXRvcGxheWluZyAoZGVmYXVsdHMgdG8gbm8pLlxuICAgKi9cbiAgbWVkaWFQbGF5YmFja1JlcXVpcmVzVXNlckFjdGlvbj86ICd5ZXMnIHwgJ25vJztcbiAgLyoqXG4gICAqIChBbmRyb2lkKSBTZXQgdG8gYSB2YWxpZCBoZXggY29sb3Igc3RyaW5nLCBmb3IgZXhhbXBsZTogIzAwZmYwMCwgYW5kIGl0IHdpbGwgY2hhbmdlIHRoZSBjb2xvciBvZiBib3RoIG5hdmlnYXRpb24gYnV0dG9ucyBmcm9tIGRlZmF1bHQuIE9ubHkgaGFzIGVmZmVjdCBpZiB1c2VyIGhhcyBsb2NhdGlvbiBzZXQgdG8geWVzIGFuZCBub3QgaGlkZW5hdmlnYXRpb25idXR0b25zIHNldCB0byB5ZXMuXG4gICAqIChpT1MpIFNldCBhcyBhIHZhbGlkIGhleCBjb2xvciBzdHJpbmcsIGZvciBleGFtcGxlOiAjMDBmZjAwLCB0byBjaGFuZ2UgZnJvbSB0aGUgZGVmYXVsdCBjb2xvci4gT25seSBhcHBsaWNhYmxlIGlmIG5hdmlnYXRpb24gYnV0dG9ucyBhcmUgdmlzaWJsZS5cbiAgICovXG4gIG5hdmlnYXRpb25idXR0b25jb2xvcj86IHN0cmluZztcbiAgLyoqIChpT1MgT25seSkgU2V0IHRvIHBhZ2VzaGVldCwgZm9ybXNoZWV0IG9yIGZ1bGxzY3JlZW4gdG8gc2V0IHRoZSBwcmVzZW50YXRpb24gc3R5bGUgKGRlZmF1bHRzIHRvIGZ1bGxzY3JlZW4pLiAqL1xuICBwcmVzZW50YXRpb25zdHlsZT86ICdwYWdlc2hlZXQnIHwgJ2Zvcm1zaGVldCcgfCAnZnVsbHNjcmVlbic7XG4gIC8qKiAoQW5kcm9pZCBPbmx5KSBTZXQgdG8geWVzIHRvIG1ha2UgSW5BcHBCcm93c2VyIFdlYlZpZXcgdG8gcGF1c2UvcmVzdW1lIHdpdGggdGhlIGFwcCB0byBzdG9wIGJhY2tncm91bmQgYXVkaW8gKHRoaXMgbWF5IGJlIHJlcXVpcmVkIHRvIGF2b2lkIEdvb2dsZSBQbGF5IGlzc3VlcykgKi9cbiAgc2hvdWxkUGF1c2VPblN1c3BlbmQ/OiAneWVzJyB8ICdubyc7XG4gIC8qKiAoaU9TIE9ubHkpIFNldCB0byB5ZXMgb3Igbm8gdG8gd2FpdCB1bnRpbCBhbGwgbmV3IHZpZXcgY29udGVudCBpcyByZWNlaXZlZCBiZWZvcmUgYmVpbmcgcmVuZGVyZWQgKGRlZmF1bHRzIHRvIG5vKS4gKi9cbiAgc3VwcHJlc3Nlc0luY3JlbWVudGFsUmVuZGVyaW5nPzogJ3llcycgfCAnbm8nO1xuICAvKiogKGlPUyBPbmx5KSBTZXQgdG8geWVzIG9yIG5vIHRvIHR1cm4gdGhlIHRvb2xiYXIgb24gb3Igb2ZmIGZvciB0aGUgSW5BcHBCcm93c2VyIChkZWZhdWx0cyB0byB5ZXMpICovXG4gIHRvb2xiYXI/OiAneWVzJyB8ICdubyc7XG4gIC8qKlxuICAgKiAoQW5kcm9pZCkgU2V0IHRvIGEgdmFsaWQgaGV4IGNvbG9yIHN0cmluZywgZm9yIGV4YW1wbGU6ICMwMGZmMDAsIGFuZCBpdCB3aWxsIGNoYW5nZSB0aGUgY29sb3IgdGhlIHRvb2xiYXIgZnJvbSBkZWZhdWx0LiBPbmx5IGhhcyBlZmZlY3QgaWYgdXNlciBoYXMgbG9jYXRpb24gc2V0IHRvIHllcy5cbiAgICogKGlPUykgU2V0IGFzIGEgdmFsaWQgaGV4IGNvbG9yIHN0cmluZywgZm9yIGV4YW1wbGU6ICMwMGZmMDAsIHRvIGNoYW5nZSBmcm9tIHRoZSBkZWZhdWx0IGNvbG9yIG9mIHRoZSB0b29sYmFyLiBPbmx5IGFwcGxpY2FibGUgaWYgdG9vbGJhciBpcyBub3QgZGlzYWJsZWQuXG4gICAqL1xuICB0b29sYmFyY29sb3I/OiBzdHJpbmc7XG4gIC8qKiAoaU9TIE9ubHkpIFNldCB0byB0b3Agb3IgYm90dG9tIChkZWZhdWx0IGlzIGJvdHRvbSkuIENhdXNlcyB0aGUgdG9vbGJhciB0byBiZSBhdCB0aGUgdG9wIG9yIGJvdHRvbSBvZiB0aGUgd2luZG93LiAqL1xuICB0b29sYmFycG9zaXRpb24/OiAndG9wJyB8ICdib3R0b20nO1xuICAvKiogKGlPUyBPbmx5KSBTZXQgdG8geWVzIG9yIG5vIHRvIG1ha2UgdGhlIHRvb2xiYXIgdHJhbnNsdWNlbnQoc2VtaS10cmFuc3BhcmVudCkgKGRlZmF1bHRzIHRvIHllcykuIE9ubHkgYXBwbGljYWJsZSBpZiB0b29sYmFyIGlzIG5vdCBkaXNhYmxlZC4gKi9cbiAgdG9vbGJhcnRyYW5zbHVjZW50PzogJ3llcycgfCAnbm8nO1xuICAvKiogKGlPUyBPbmx5KSBTZXQgdG8gZmxpcGhvcml6b250YWwsIGNyb3NzZGlzc29sdmUgb3IgY292ZXJ2ZXJ0aWNhbCB0byBzZXQgdGhlIHRyYW5zaXRpb24gc3R5bGUgKGRlZmF1bHRzIHRvIGNvdmVydmVydGljYWwpLiAqL1xuICB0cmFuc2l0aW9uc3R5bGU/OiAnZmxpcGhvcml6b250YWwnIHwgJ2Nyb3NzZGlzc29sdmUnIHwgJ2NvdmVydmVydGljYWwnO1xuICAvKiogKEFuZHJvaWQgT25seSkgU2V0cyB3aGV0aGVyIHRoZSBXZWJWaWV3IHNob3VsZCBlbmFibGUgc3VwcG9ydCBmb3IgdGhlIFwidmlld3BvcnRcIiBIVE1MIG1ldGEgdGFnIG9yIHNob3VsZCB1c2UgYSB3aWRlIHZpZXdwb3J0LiBXaGVuIHRoZSB2YWx1ZSBvZiB0aGUgc2V0dGluZyBpcyBubywgdGhlIGxheW91dCB3aWR0aCBpcyBhbHdheXMgc2V0IHRvIHRoZSB3aWR0aCBvZiB0aGUgV2ViVmlldyBjb250cm9sIGluIGRldmljZS1pbmRlcGVuZGVudCAoQ1NTKSBwaXhlbHMuIFdoZW4gdGhlIHZhbHVlIGlzIHllcyBhbmQgdGhlIHBhZ2UgY29udGFpbnMgdGhlIHZpZXdwb3J0IG1ldGEgdGFnLCB0aGUgdmFsdWUgb2YgdGhlIHdpZHRoIHNwZWNpZmllZCBpbiB0aGUgdGFnIGlzIHVzZWQuIElmIHRoZSBwYWdlIGRvZXMgbm90IGNvbnRhaW4gdGhlIHRhZyBvciBkb2VzIG5vdCBwcm92aWRlIGEgd2lkdGgsIHRoZW4gYSB3aWRlIHZpZXdwb3J0IHdpbGwgYmUgdXNlZC4gKGRlZmF1bHRzIHRvIHllcykuICovXG4gIHVzZVdpZGVWaWV3UG9ydD86ICd5ZXMnIHwgJ25vJztcbiAgLyoqIChpT1MgT25seSkgU2V0IHRvIHllcyB0byB1c2UgV0tXZWJWaWV3IGVuZ2luZSBmb3IgdGhlIEluYXBwQnJvd3Nlci4gT21pdCBvciBzZXQgdG8gbm8gKGRlZmF1bHQpIHRvIHVzZSBVSVdlYlZpZXcuICovXG4gIHVzZXdrd2Vidmlldz86ICd5ZXMnIHwgJ25vJztcbiAgLyoqIChBbmRyb2lkIE9ubHkpIFNldCB0byB5ZXMgdG8gc2hvdyBBbmRyb2lkIGJyb3dzZXIncyB6b29tIGNvbnRyb2xzLCBzZXQgdG8gbm8gdG8gaGlkZSB0aGVtLiBEZWZhdWx0IHZhbHVlIGlzIHllcy4gKi9cbiAgem9vbT86ICd5ZXMnIHwgJ25vJztcbiAgLyoqXG4gICAqIEBoaWRkZW5cbiAgICovXG4gIFtrZXk6IHN0cmluZ106IGFueTtcbn1cblxuZXhwb3J0IHR5cGUgSW5BcHBCcm93c2VyRXZlbnRUeXBlID1cbiAgfCAnbG9hZHN0YXJ0J1xuICB8ICdsb2Fkc3RvcCdcbiAgfCAnbG9hZGVycm9yJ1xuICB8ICdleGl0J1xuICB8ICdiZWZvcmVsb2FkJ1xuICB8ICdtZXNzYWdlJ1xuICB8ICdjdXN0b21zY2hlbWUnO1xuXG5leHBvcnQgaW50ZXJmYWNlIEluQXBwQnJvd3NlckV2ZW50IGV4dGVuZHMgRXZlbnQge1xuICAvKiogdGhlIGV2ZW50IG5hbWUgKi9cbiAgdHlwZTogc3RyaW5nO1xuICAvKiogdGhlIFVSTCB0aGF0IHdhcyBsb2FkZWQuICovXG4gIHVybDogc3RyaW5nO1xuICAvKiogdGhlIGVycm9yIGNvZGUsIG9ubHkgaW4gdGhlIGNhc2Ugb2YgbG9hZGVycm9yLiAqL1xuICBjb2RlOiBudW1iZXI7XG4gIC8qKiB0aGUgZXJyb3IgbWVzc2FnZSwgb25seSBpbiB0aGUgY2FzZSBvZiBsb2FkZXJyb3IuICovXG4gIG1lc3NhZ2U6IHN0cmluZztcbiAgLyoqIHRoZSBwb3N0TWVzc2FnZSBkYXRhLCBvbmx5IGluIHRoZSBjYXNlIG9mIG1lc3NhZ2UuICovXG4gIGRhdGE6IGFueTtcbn1cblxuLyoqXG4gKiBAaGlkZGVuXG4gKi9cbmV4cG9ydCBjbGFzcyBJbkFwcEJyb3dzZXJPYmplY3Qge1xuICBwcml2YXRlIF9vYmplY3RJbnN0YW5jZTogYW55O1xuXG4gIC8qKlxuICAgKiBPcGVucyBhIFVSTCBpbiBhIG5ldyBJbkFwcEJyb3dzZXIgaW5zdGFuY2UsIHRoZSBjdXJyZW50IGJyb3dzZXIgaW5zdGFuY2UsIG9yIHRoZSBzeXN0ZW0gYnJvd3Nlci5cbiAgICogQHBhcmFtIHtzdHJpbmd9IHVybCAgICAgVGhlIFVSTCB0byBsb2FkLlxuICAgKiBAcGFyYW0ge3N0cmluZ30gW3RhcmdldD1cInNlbGZcIl0gIFRoZSB0YXJnZXQgaW4gd2hpY2ggdG8gbG9hZCB0aGUgVVJMLCBhbiBvcHRpb25hbCBwYXJhbWV0ZXIgdGhhdCBkZWZhdWx0cyB0byBfc2VsZi5cbiAgICogICAgICAgICAgICAgICAgIF9zZWxmOiBPcGVucyBpbiB0aGUgV2ViVmlldyBpZiB0aGUgVVJMIGlzIGluIHRoZSB3aGl0ZSBsaXN0LCBvdGhlcndpc2UgaXQgb3BlbnMgaW4gdGhlIEluQXBwQnJvd3Nlci5cbiAgICogICAgICAgICAgICAgICAgIF9ibGFuazogT3BlbnMgaW4gdGhlIEluQXBwQnJvd3Nlci5cbiAgICogICAgICAgICAgICAgICAgIF9zeXN0ZW06IE9wZW5zIGluIHRoZSBzeXN0ZW0ncyB3ZWIgYnJvd3Nlci5cbiAgICogQHBhcmFtIHtzdHJpbmcgfCBJbkFwcEJyb3dzZXJPcHRpb25zfSBbb3B0aW9uc10gT3B0aW9ucyBmb3IgdGhlIEluQXBwQnJvd3Nlci4gT3B0aW9uYWwsIGRlZmF1bHRpbmcgdG86IGxvY2F0aW9uPXllcy5cbiAgICogICAgICAgICAgICAgICAgIFRoZSBvcHRpb25zIHN0cmluZyBtdXN0IG5vdCBjb250YWluIGFueSBibGFuayBzcGFjZSwgYW5kIGVhY2ggZmVhdHVyZSdzXG4gICAqICAgICAgICAgICAgICAgICBuYW1lL3ZhbHVlIHBhaXJzIG11c3QgYmUgc2VwYXJhdGVkIGJ5IGEgY29tbWEuIEZlYXR1cmUgbmFtZXMgYXJlIGNhc2UgaW5zZW5zaXRpdmUuXG4gICAqL1xuICBjb25zdHJ1Y3Rvcih1cmw6IHN0cmluZywgdGFyZ2V0Pzogc3RyaW5nLCBvcHRpb25zPzogc3RyaW5nIHwgSW5BcHBCcm93c2VyT3B0aW9ucykge1xuICAgIHRyeSB7XG4gICAgICBpZiAob3B0aW9ucyAmJiB0eXBlb2Ygb3B0aW9ucyAhPT0gJ3N0cmluZycpIHtcbiAgICAgICAgb3B0aW9ucyA9IE9iamVjdC5rZXlzKG9wdGlvbnMpXG4gICAgICAgICAgLm1hcCgoa2V5OiBzdHJpbmcpID0+IGAke2tleX09JHsob3B0aW9ucyBhcyBJbkFwcEJyb3dzZXJPcHRpb25zKVtrZXldfWApXG4gICAgICAgICAgLmpvaW4oJywnKTtcbiAgICAgIH1cblxuICAgICAgdGhpcy5fb2JqZWN0SW5zdGFuY2UgPSBjb3Jkb3ZhLkluQXBwQnJvd3Nlci5vcGVuKHVybCwgdGFyZ2V0LCBvcHRpb25zKTtcbiAgICB9IGNhdGNoIChlKSB7XG4gICAgICBpZiAodHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgd2luZG93Lm9wZW4odXJsLCB0YXJnZXQpO1xuICAgICAgfVxuICAgICAgY29uc29sZS53YXJuKFxuICAgICAgICAnTmF0aXZlOiBJbkFwcEJyb3dzZXIgaXMgbm90IGluc3RhbGxlZCBvciB5b3UgYXJlIHJ1bm5pbmcgb24gYSBicm93c2VyLiBGYWxsaW5nIGJhY2sgdG8gd2luZG93Lm9wZW4uJ1xuICAgICAgKTtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogTWV0aG9kIHRvIGJlIGNhbGxlZCBhZnRlciB0aGUgXCJiZWZvcmVsb2FkXCIgZXZlbnQgdG8gY29udGludWUgdGhlIHNjcmlwdFxuICAgKiBAcGFyYW0gc3RyVXJsIHtTdHJpbmd9IFRoZSBVUkwgdGhlIEluQXBwQnJvd3NlciBzaG91bGQgbmF2aWdhdGUgdG8uXG4gICAqL1xuICBAQ29yZG92YUluc3RhbmNlKHsgc3luYzogdHJ1ZSB9KVxuICBfbG9hZEFmdGVyQmVmb3JlbG9hZChzdHJVcmw6IHN0cmluZyk6IHZvaWQge31cblxuICAvKipcbiAgICogRGlzcGxheXMgYW4gSW5BcHBCcm93c2VyIHdpbmRvdyB0aGF0IHdhcyBvcGVuZWQgaGlkZGVuLiBDYWxsaW5nIHRoaXMgaGFzIG5vIGVmZmVjdFxuICAgKiBpZiB0aGUgSW5BcHBCcm93c2VyIHdhcyBhbHJlYWR5IHZpc2libGUuXG4gICAqL1xuICBAQ29yZG92YUluc3RhbmNlKHsgc3luYzogdHJ1ZSB9KVxuICBzaG93KCk6IHZvaWQge31cblxuICAvKipcbiAgICogQ2xvc2VzIHRoZSBJbkFwcEJyb3dzZXIgd2luZG93LlxuICAgKi9cbiAgQENvcmRvdmFJbnN0YW5jZSh7IHN5bmM6IHRydWUgfSlcbiAgY2xvc2UoKTogdm9pZCB7fVxuXG4gIC8qKlxuICAgKiBIaWRlcyBhbiBJbkFwcEJyb3dzZXIgd2luZG93IHRoYXQgaXMgY3VycmVudGx5IHNob3duLiBDYWxsaW5nIHRoaXMgaGFzIG5vIGVmZmVjdFxuICAgKiBpZiB0aGUgSW5BcHBCcm93c2VyIHdhcyBhbHJlYWR5IGhpZGRlbi5cbiAgICovXG4gIEBDb3Jkb3ZhSW5zdGFuY2UoeyBzeW5jOiB0cnVlIH0pXG4gIGhpZGUoKTogdm9pZCB7fVxuXG4gIC8qKlxuICAgKiBJbmplY3RzIEphdmFTY3JpcHQgY29kZSBpbnRvIHRoZSBJbkFwcEJyb3dzZXIgd2luZG93LlxuICAgKiBAcGFyYW0gc2NyaXB0IHtPYmplY3R9IERldGFpbHMgb2YgdGhlIHNjcmlwdCB0byBydW4sIHNwZWNpZnlpbmcgZWl0aGVyIGEgZmlsZSBvciBjb2RlIGtleS5cbiAgICogQHJldHVybnMge1Byb21pc2U8YW55Pn1cbiAgICovXG4gIEBDb3Jkb3ZhSW5zdGFuY2UoKVxuICBleGVjdXRlU2NyaXB0KHNjcmlwdDogeyBmaWxlPzogc3RyaW5nOyBjb2RlPzogc3RyaW5nIH0pOiBQcm9taXNlPGFueT4ge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8qKlxuICAgKiBJbmplY3RzIENTUyBpbnRvIHRoZSBJbkFwcEJyb3dzZXIgd2luZG93LlxuICAgKiBAcGFyYW0gY3NzIHtPYmplY3R9IERldGFpbHMgb2YgdGhlIHNjcmlwdCB0byBydW4sIHNwZWNpZnlpbmcgZWl0aGVyIGEgZmlsZSBvciBjb2RlIGtleS5cbiAgICogQHJldHVybnMge1Byb21pc2U8YW55Pn1cbiAgICovXG4gIEBDb3Jkb3ZhSW5zdGFuY2UoKVxuICBpbnNlcnRDU1MoY3NzOiB7IGZpbGU/OiBzdHJpbmc7IGNvZGU/OiBzdHJpbmcgfSk6IFByb21pc2U8YW55PiB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgLyoqXG4gICAqIEEgbWV0aG9kIHRoYXQgYWxsb3dzIHlvdSB0byBsaXN0ZW4gdG8gZXZlbnRzIGhhcHBlbmluZyBpbiB0aGUgYnJvd3Nlci5cbiAgICogQHBhcmFtIGV2ZW50IHtJbkFwcEJyb3dzZXJFdmVudFR5cGV9IE5hbWUgb2YgdGhlIGV2ZW50XG4gICAqIEByZXR1cm5zIHtPYnNlcnZhYmxlPEluQXBwQnJvd3NlckV2ZW50Pn0gUmV0dXJucyBiYWNrIGFuIG9ic2VydmFibGUgdGhhdCB3aWxsIGxpc3RlbiB0byB0aGUgZXZlbnQgb24gc3Vic2NyaWJlLCBhbmQgd2lsbCBzdG9wIGxpc3RlbmluZyB0byB0aGUgZXZlbnQgb24gdW5zdWJzY3JpYmUuXG4gICAqL1xuICBASW5zdGFuY2VDaGVjaygpXG4gIG9uKGV2ZW50OiBJbkFwcEJyb3dzZXJFdmVudFR5cGUpOiBPYnNlcnZhYmxlPEluQXBwQnJvd3NlckV2ZW50PiB7XG4gICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlPEluQXBwQnJvd3NlckV2ZW50Pigob2JzZXJ2ZXI6IE9ic2VydmVyPEluQXBwQnJvd3NlckV2ZW50PikgPT4ge1xuICAgICAgdGhpcy5fb2JqZWN0SW5zdGFuY2UuYWRkRXZlbnRMaXN0ZW5lcihldmVudCwgb2JzZXJ2ZXIubmV4dC5iaW5kKG9ic2VydmVyKSk7XG4gICAgICByZXR1cm4gKCkgPT4gdGhpcy5fb2JqZWN0SW5zdGFuY2UucmVtb3ZlRXZlbnRMaXN0ZW5lcihldmVudCwgb2JzZXJ2ZXIubmV4dC5iaW5kKG9ic2VydmVyKSk7XG4gICAgfSk7XG4gIH1cblxuICAvKipcbiAgICogQSBtZXRob2QgdGhhdCBhbGxvd3MgeW91IHRvIGxpc3RlbiB0byBldmVudHMgaGFwcGVuaW5nIGluIHRoZSBicm93c2VyLlxuICAgKiBAcGFyYW0gZXZlbnQge3N0cmluZ30gTmFtZSBvZiB0aGUgZXZlbnRcbiAgICogQHJldHVybnMge09ic2VydmFibGU8SW5BcHBCcm93c2VyRXZlbnQ+fSBSZXR1cm5zIGJhY2sgYW4gb2JzZXJ2YWJsZSB0aGF0IHdpbGwgbGlzdGVuIHRvIHRoZSBldmVudCBvbiBzdWJzY3JpYmUsIGFuZCB3aWxsIHN0b3AgbGlzdGVuaW5nIHRvIHRoZSBldmVudCBvbiB1bnN1YnNjcmliZS5cbiAgICovXG4gIEBJbnN0YW5jZUNoZWNrKClcbiAgb24oZXZlbnQ6IHN0cmluZyk6IE9ic2VydmFibGU8SW5BcHBCcm93c2VyRXZlbnQ+IHtcbiAgICByZXR1cm4gbmV3IE9ic2VydmFibGU8SW5BcHBCcm93c2VyRXZlbnQ+KChvYnNlcnZlcjogT2JzZXJ2ZXI8SW5BcHBCcm93c2VyRXZlbnQ+KSA9PiB7XG4gICAgICB0aGlzLl9vYmplY3RJbnN0YW5jZS5hZGRFdmVudExpc3RlbmVyKGV2ZW50LCBvYnNlcnZlci5uZXh0LmJpbmQob2JzZXJ2ZXIpKTtcbiAgICAgIHJldHVybiAoKSA9PiB0aGlzLl9vYmplY3RJbnN0YW5jZS5yZW1vdmVFdmVudExpc3RlbmVyKGV2ZW50LCBvYnNlcnZlci5uZXh0LmJpbmQob2JzZXJ2ZXIpKTtcbiAgICB9KTtcbiAgfVxufVxuXG4vKipcbiAqIEBuYW1lIEluIEFwcCBCcm93c2VyXG4gKiBAZGVzY3JpcHRpb24gTGF1bmNoZXMgaW4gYXBwIEJyb3dzZXJcbiAqIEB1c2FnZVxuICogYGBgdHlwZXNjcmlwdFxuICogaW1wb3J0IHsgSW5BcHBCcm93c2VyIH0gZnJvbSAnQGlvbmljLW5hdGl2ZS9pbi1hcHAtYnJvd3Nlci9uZ3gnO1xuICpcbiAqIGNvbnN0cnVjdG9yKHByaXZhdGUgaWFiOiBJbkFwcEJyb3dzZXIpIHsgfVxuICpcbiAqXG4gKiAuLi5cbiAqXG4gKlxuICogY29uc3QgYnJvd3NlciA9IHRoaXMuaWFiLmNyZWF0ZSgnaHR0cHM6Ly9pb25pY2ZyYW1ld29yay5jb20vJyk7XG4gKlxuICogYnJvd3Nlci5leGVjdXRlU2NyaXB0KC4uLik7XG4gKlxuICogYnJvd3Nlci5pbnNlcnRDU1MoLi4uKTtcbiAqIGJyb3dzZXIub24oJ2xvYWRzdG9wJykuc3Vic2NyaWJlKGV2ZW50ID0+IHtcbiAqICAgIGJyb3dzZXIuaW5zZXJ0Q1NTKHsgY29kZTogXCJib2R5e2NvbG9yOiByZWQ7XCIgfSk7XG4gKiB9KTtcbiAqXG4gKiBicm93c2VyLmNsb3NlKCk7XG4gKlxuICogYGBgXG4gKiBAY2xhc3Nlc1xuICogSW5BcHBCcm93c2VyT2JqZWN0XG4gKiBAaW50ZXJmYWNlc1xuICogSW5BcHBCcm93c2VyRXZlbnRcbiAqIEluQXBwQnJvd3Nlck9wdGlvbnNcbiAqL1xuQFBsdWdpbih7XG4gIHBsdWdpbk5hbWU6ICdJbkFwcEJyb3dzZXInLFxuICBwbHVnaW46ICdjb3Jkb3ZhLXBsdWdpbi1pbmFwcGJyb3dzZXInLFxuICBwbHVnaW5SZWY6ICdjb3Jkb3ZhLkluQXBwQnJvd3NlcicsXG4gIHJlcG86ICdodHRwczovL2dpdGh1Yi5jb20vYXBhY2hlL2NvcmRvdmEtcGx1Z2luLWluYXBwYnJvd3NlcicsXG4gIHBsYXRmb3JtczogWydBbWF6b25GaXJlIE9TJywgJ0FuZHJvaWQnLCAnQnJvd3NlcicsICdpT1MnLCAnbWFjT1MnLCAnV2luZG93cyddLFxufSlcbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBJbkFwcEJyb3dzZXIgZXh0ZW5kcyBJb25pY05hdGl2ZVBsdWdpbiB7XG4gIC8qKlxuICAgKiBPcGVucyBhIFVSTCBpbiBhIG5ldyBJbkFwcEJyb3dzZXIgaW5zdGFuY2UsIHRoZSBjdXJyZW50IGJyb3dzZXIgaW5zdGFuY2UsIG9yIHRoZSBzeXN0ZW0gYnJvd3Nlci5cbiAgICogQHBhcmFtICB1cmwge3N0cmluZ30gICAgIFRoZSBVUkwgdG8gbG9hZC5cbiAgICogQHBhcmFtICB0YXJnZXQge3N0cmluZ30gIFRoZSB0YXJnZXQgaW4gd2hpY2ggdG8gbG9hZCB0aGUgVVJMLCBhbiBvcHRpb25hbCBwYXJhbWV0ZXIgdGhhdCBkZWZhdWx0cyB0byBfc2VsZi5cbiAgICogQHBhcmFtICBvcHRpb25zIHtzdHJpbmd9IE9wdGlvbnMgZm9yIHRoZSBJbkFwcEJyb3dzZXIuIE9wdGlvbmFsLCBkZWZhdWx0aW5nIHRvOiBsb2NhdGlvbj15ZXMuXG4gICAqICAgICAgICAgICAgICAgICBUaGUgb3B0aW9ucyBzdHJpbmcgbXVzdCBub3QgY29udGFpbiBhbnkgYmxhbmsgc3BhY2UsIGFuZCBlYWNoIGZlYXR1cmUnc1xuICAgKiAgICAgICAgICAgICAgICAgbmFtZS92YWx1ZSBwYWlycyBtdXN0IGJlIHNlcGFyYXRlZCBieSBhIGNvbW1hLiBGZWF0dXJlIG5hbWVzIGFyZSBjYXNlIGluc2Vuc2l0aXZlLlxuICAgKiBAcmV0dXJucyB7SW5BcHBCcm93c2VyT2JqZWN0fVxuICAgKi9cbiAgY3JlYXRlKHVybDogc3RyaW5nLCB0YXJnZXQ/OiBzdHJpbmcsIG9wdGlvbnM/OiBzdHJpbmcgfCBJbkFwcEJyb3dzZXJPcHRpb25zKTogSW5BcHBCcm93c2VyT2JqZWN0IHtcbiAgICByZXR1cm4gbmV3IEluQXBwQnJvd3Nlck9iamVjdCh1cmwsIHRhcmdldCwgb3B0aW9ucyk7XG4gIH1cbn1cbiJdfQ==

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button meni=\"m1\">\n        <ion-icon name=\"menu\"></ion-icon>\n      </ion-menu-button>\n    </ion-buttons>\n    <ion-title>\n      Home\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size-xl=\"6\" offset-xl=\"3\">\n        <div class=\"cssDivAboutUs\">\n          <div>\n            <div>\n              <img\n                src=\"https://www.indian-grill.com/wp-content/uploads/2016/11/IMG_0118.jpg\"\n              />\n            </div>\n            <div class=\"cssDivWhoWeAre\">\n              <h2 class=\"cssH2\">About Us</h2>\n              <p class=\"cssPAboutUs\" *ngIf=\"aboutDescription != null\">\n                {{aboutDescription.descp}}\n              </p>\n            </div>\n                <p class=\"cssPFeatured\">\n                  We serve halal meat\n                </p>\n          </div>\n          <div style=\"margin-top: 35px;\">\n            <h2 class=\"cssH2Testimonial\">Our Testimonial</h2>\n            <ion-row>\n              <ion-col style=\"text-align: center;\">\n                <ion-icon name=\"quote\" color=\"primary\" class=\"cssIconTestimonial\"></ion-icon>\n              </ion-col>\n            </ion-row>\n            <ion-slides pager=\"true\" [options]=\"slideOpts\">\n              <ion-slide *ngFor=\"let testimonial of testimonialData\">\n                <p class=\"cssPTestimonial\">\n                  <strong>{{testimonial.testimonials}}</strong>\n                  <br>\n                  <br>\n                  {{testimonial.author}}\n                </p>\n              </ion-slide>\n            </ion-slides>\n          </div>\n        </div>\n        <div *ngIf=\"resTimingData != null\">\n          <h3 class=\"cssH3\">{{resTimeTitle}}</h3>\n          <!-- <ion-row>\n            <ion-col class=\"cssColTiming\">\n              <ion-label class=\"cssLabelTiming\"><b>Open 7 days Week</b></ion-label>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col class=\"cssColTiming\">\n              <ion-label class=\"cssLabelTiming\"><b>Monday to Thursday</b></ion-label>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col class=\"cssColTiming\">\n              <ion-label class=\"cssLabelTiming\">11:30 AM to 10:00 PM</ion-label>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col class=\"cssColTiming\">\n              <ion-label class=\"cssLabelTiming\"><b>Friday and Saturday</b></ion-label>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col class=\"cssColTiming\">\n              <ion-label class=\"cssLabelTiming\">11:30 AM to 10:30 PM</ion-label>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col class=\"cssColTiming\">\n              <ion-label class=\"cssLabelTiming\"><b>Sunday</b></ion-label>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col class=\"cssColTiming\">\n              <ion-label class=\"cssLabelTiming\">11:30 AM to 09:30 PM</ion-label>\n            </ion-col>\n          </ion-row> -->\n          <ion-row *ngFor=\"let arr of resTimingData\">\n            <ion-col class=\"cssColTiming\">\n              <ion-label class=\"cssLabelTiming\" [innerHTML]=\"arr\"></ion-label>\n            </ion-col>\n          </ion-row>\n        </div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size-xl=\"6\" offset-xl=\"3\">\n        <ion-button expand=\"block\" color=\"primary\" (click)=\"inAppB()\">\n          Order Now\n        </ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/home/home-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/home/home-routing.module.ts ***!
  \*********************************************/
/*! exports provided: HomePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageRoutingModule", function() { return HomePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");




var routes = [
    {
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_3__["HomePage"],
    }
];
var HomePageRoutingModule = /** @class */ (function () {
    function HomePageRoutingModule() {
    }
    HomePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], HomePageRoutingModule);
    return HomePageRoutingModule;
}());



/***/ }),

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ "./node_modules/@ionic-native/in-app-browser/ngx/index.js");
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./home-routing.module */ "./src/app/home/home-routing.module.ts");








var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
                _home_routing_module__WEBPACK_IMPORTED_MODULE_7__["HomePageRoutingModule"]
            ],
            declarations: [_home_page__WEBPACK_IMPORTED_MODULE_5__["HomePage"]],
            providers: [_ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_6__["InAppBrowser"]]
        })
    ], HomePageModule);
    return HomePageModule;
}());



/***/ }),

/***/ "./src/app/home/home.page.scss":
/*!*************************************!*\
  !*** ./src/app/home/home.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".cssDivAboutUs {\n  background: #f1f1f1;\n}\n\n.cssDivWhoWeAre {\n  padding: 30px 30px 20px;\n}\n\n.cssH2 {\n  color: #e2001a;\n  margin: 0 0 15px;\n  font-size: 22px;\n  font-weight: 800;\n  line-height: 1.3;\n}\n\np {\n  line-height: 22px;\n  margin: 0 0 10px;\n}\n\n.cssPAboutUs {\n  color: #666;\n  font-size: 13px;\n  letter-spacing: 0.5px;\n}\n\n.cssPFeatured {\n  color: #666;\n  font-style: italic;\n  font-weight: 700;\n  margin-top: 20px;\n  text-transform: uppercase;\n  text-align: center;\n  font-size: 32px;\n}\n\nion-slides {\n  height: 160px;\n}\n\n.cssH2Testimonial {\n  color: #000000;\n  margin: 0 0 15px;\n  font-size: 22px;\n  font-weight: 800;\n  line-height: 1.3;\n  text-align: center;\n}\n\n.cssIconTestimonial {\n  font-size: 40px;\n}\n\n.cssPTestimonial {\n  color: #666;\n  padding: 5px 10px 0;\n  font-size: 15px;\n  font-weight: 500;\n  letter-spacing: 0.7px;\n  opacity: 1;\n  text-align: center;\n}\n\n.cssH3 {\n  padding: 0;\n  text-align: center;\n  text-transform: uppercase;\n  color: #000;\n  font-size: 18px;\n  font-weight: 800;\n  line-height: 18px;\n  margin: 30px 0 25px;\n}\n\n.cssColTiming {\n  text-align: center;\n  margin: 8px;\n}\n\n.cssLabelTiming {\n  font-size: 15px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9ha2Fhc2hkZXYvUHJvZ3JhbW1pbmcvT2ZmaWNlV29yay9pbmRpYW4tZ3JpbGwvc3JjL2FwcC9ob21lL2hvbWUucGFnZS5zY3NzIiwic3JjL2FwcC9ob21lL2hvbWUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsbUJBQUE7QUNDRjs7QURFQTtFQUNFLHVCQUFBO0FDQ0Y7O0FERUE7RUFDRSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtBQ0NGOztBREVBO0VBQ0UsaUJBQUE7RUFDQSxnQkFBQTtBQ0NGOztBREVBO0VBQ0UsV0FBQTtFQUNBLGVBQUE7RUFDQSxxQkFBQTtBQ0NGOztBREVBO0VBQ0UsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7RUFHQSxlQUFBO0FDREY7O0FESUE7RUFDRSxhQUFBO0FDREY7O0FESUE7RUFDRSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FDREY7O0FESUE7RUFDRSxlQUFBO0FDREY7O0FESUE7RUFDRSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxxQkFBQTtFQUNBLFVBQUE7RUFFQSxrQkFBQTtBQ0ZGOztBREtBO0VBQ0UsVUFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtBQ0ZGOztBREtBO0VBQ0Usa0JBQUE7RUFDQSxXQUFBO0FDRkY7O0FES0E7RUFDRSxlQUFBO0FDRkYiLCJmaWxlIjoic3JjL2FwcC9ob21lL2hvbWUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNzc0RpdkFib3V0VXMge1xuICBiYWNrZ3JvdW5kOiAjZjFmMWYxO1xufVxuXG4uY3NzRGl2V2hvV2VBcmUge1xuICBwYWRkaW5nOiAzMHB4IDMwcHggMjBweDtcbn1cblxuLmNzc0gyIHtcbiAgY29sb3I6ICNlMjAwMWE7XG4gIG1hcmdpbjogMCAwIDE1cHg7XG4gIGZvbnQtc2l6ZTogMjJweDtcbiAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgbGluZS1oZWlnaHQ6IDEuMztcbn1cblxucCB7XG4gIGxpbmUtaGVpZ2h0OiAyMnB4O1xuICBtYXJnaW46IDAgMCAxMHB4O1xufVxuXG4uY3NzUEFib3V0VXMge1xuICBjb2xvcjogIzY2NjtcbiAgZm9udC1zaXplOiAxM3B4O1xuICBsZXR0ZXItc3BhY2luZzogMC41cHg7XG59XG5cbi5jc3NQRmVhdHVyZWQge1xuICBjb2xvcjogIzY2NjtcbiAgZm9udC1zdHlsZTogaXRhbGljO1xuICBmb250LXdlaWdodDogNzAwO1xuICBtYXJnaW4tdG9wOiAyMHB4O1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIC8vIGZvbnQtc2l6ZTogNDBweDtcbiAgLy8gZm9udC1mYW1pbHk6IFwiU2Vhd2VlZCBTY3JpcHRcIjtcbiAgZm9udC1zaXplOiAzMnB4O1xufVxuXG5pb24tc2xpZGVzIHtcbiAgaGVpZ2h0OiAxNjBweDtcbn1cblxuLmNzc0gyVGVzdGltb25pYWwge1xuICBjb2xvcjogIzAwMDAwMDtcbiAgbWFyZ2luOiAwIDAgMTVweDtcbiAgZm9udC1zaXplOiAyMnB4O1xuICBmb250LXdlaWdodDogODAwO1xuICBsaW5lLWhlaWdodDogMS4zO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5jc3NJY29uVGVzdGltb25pYWwge1xuICBmb250LXNpemU6IDQwcHg7XG59XG5cbi5jc3NQVGVzdGltb25pYWwge1xuICBjb2xvcjogIzY2NjtcbiAgcGFkZGluZzogNXB4IDEwcHggMDtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBmb250LXdlaWdodDogNTAwO1xuICBsZXR0ZXItc3BhY2luZzogMC43cHg7XG4gIG9wYWNpdHk6IDE7XG4gIC8vIG92ZXJmbG93OiBoaWRkZW47XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmNzc0gzIHtcbiAgcGFkZGluZzogMDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBjb2xvcjogIzAwMDtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBmb250LXdlaWdodDogODAwO1xuICBsaW5lLWhlaWdodDogMThweDtcbiAgbWFyZ2luOiAzMHB4IDAgMjVweDtcbn1cblxuLmNzc0NvbFRpbWluZyB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luOiA4cHg7XG59XG5cbi5jc3NMYWJlbFRpbWluZyB7XG4gIGZvbnQtc2l6ZTogMTVweDtcbn1cbiIsIi5jc3NEaXZBYm91dFVzIHtcbiAgYmFja2dyb3VuZDogI2YxZjFmMTtcbn1cblxuLmNzc0Rpdldob1dlQXJlIHtcbiAgcGFkZGluZzogMzBweCAzMHB4IDIwcHg7XG59XG5cbi5jc3NIMiB7XG4gIGNvbG9yOiAjZTIwMDFhO1xuICBtYXJnaW46IDAgMCAxNXB4O1xuICBmb250LXNpemU6IDIycHg7XG4gIGZvbnQtd2VpZ2h0OiA4MDA7XG4gIGxpbmUtaGVpZ2h0OiAxLjM7XG59XG5cbnAge1xuICBsaW5lLWhlaWdodDogMjJweDtcbiAgbWFyZ2luOiAwIDAgMTBweDtcbn1cblxuLmNzc1BBYm91dFVzIHtcbiAgY29sb3I6ICM2NjY7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgbGV0dGVyLXNwYWNpbmc6IDAuNXB4O1xufVxuXG4uY3NzUEZlYXR1cmVkIHtcbiAgY29sb3I6ICM2NjY7XG4gIGZvbnQtc3R5bGU6IGl0YWxpYztcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDMycHg7XG59XG5cbmlvbi1zbGlkZXMge1xuICBoZWlnaHQ6IDE2MHB4O1xufVxuXG4uY3NzSDJUZXN0aW1vbmlhbCB7XG4gIGNvbG9yOiAjMDAwMDAwO1xuICBtYXJnaW46IDAgMCAxNXB4O1xuICBmb250LXNpemU6IDIycHg7XG4gIGZvbnQtd2VpZ2h0OiA4MDA7XG4gIGxpbmUtaGVpZ2h0OiAxLjM7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmNzc0ljb25UZXN0aW1vbmlhbCB7XG4gIGZvbnQtc2l6ZTogNDBweDtcbn1cblxuLmNzc1BUZXN0aW1vbmlhbCB7XG4gIGNvbG9yOiAjNjY2O1xuICBwYWRkaW5nOiA1cHggMTBweCAwO1xuICBmb250LXNpemU6IDE1cHg7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIGxldHRlci1zcGFjaW5nOiAwLjdweDtcbiAgb3BhY2l0eTogMTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uY3NzSDMge1xuICBwYWRkaW5nOiAwO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGNvbG9yOiAjMDAwO1xuICBmb250LXNpemU6IDE4cHg7XG4gIGZvbnQtd2VpZ2h0OiA4MDA7XG4gIGxpbmUtaGVpZ2h0OiAxOHB4O1xuICBtYXJnaW46IDMwcHggMCAyNXB4O1xufVxuXG4uY3NzQ29sVGltaW5nIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW46IDhweDtcbn1cblxuLmNzc0xhYmVsVGltaW5nIHtcbiAgZm9udC1zaXplOiAxNXB4O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/home/home.page.ts":
/*!***********************************!*\
  !*** ./src/app/home/home.page.ts ***!
  \***********************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ "./node_modules/@ionic-native/in-app-browser/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_api_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/api-service.service */ "./src/app/services/api-service.service.ts");



// import { Plugins } from '@capacitor/core';


// const { App } = Plugins;
var HomePage = /** @class */ (function () {
    function HomePage(iab, loadingCtrl, api) {
        this.iab = iab;
        this.loadingCtrl = loadingCtrl;
        this.api = api;
        this.iabOptions = {
            location: 'no',
            usewkwebview: 'yes',
            footer: 'no',
            toolbar: 'no',
            zoom: 'no'
        };
        this.slideOpts = {
            initialSlide: 0,
            speed: 400
        };
        // setTimeout(() => this.inAppB(), 5000);
        // this.getAbout();
        // this.getTestimonial();
    }
    HomePage.prototype.ngOnInit = function () {
        this.getAbout();
        this.getTestimonial();
        this.getRestaurantTiming();
    };
    HomePage.prototype.getAbout = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({})];
                    case 1:
                        loading = _a.sent();
                        loading.present();
                        this.api.aboutFn().then(function (result) {
                            var aboutData = result;
                            console.log('About: ', aboutData);
                            if (aboutData.status == 200) {
                                _this.aboutDescription = aboutData.about;
                                loading.dismiss();
                            }
                        }).catch(function (err) {
                            console.log('err', err);
                            loading.dismiss();
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.getTestimonial = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({})];
                    case 1:
                        loading = _a.sent();
                        loading.present();
                        this.api.testimonialFn().then(function (result) {
                            var testimonial = result;
                            console.log('Testimonials: ', testimonial);
                            if (testimonial.status == 200) {
                                _this.testimonialData = testimonial.testimonial;
                                loading.dismiss();
                            }
                        }).catch(function (err) {
                            console.log('err', err);
                            loading.dismiss();
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.getRestaurantTiming = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({})];
                    case 1:
                        loading = _a.sent();
                        loading.present();
                        this.api.timingFn().then(function (result) {
                            var data = result;
                            console.log('Timing: ', data);
                            if (data.status == 200) {
                                _this.resTimeTitle = data.timing.title;
                                _this.resTimingData = data.timing.descp;
                                loading.dismiss();
                            }
                        }).catch(function (err) {
                            console.log('err', err);
                            loading.dismiss();
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.inAppB = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var browser;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                browser = this.iab.create('https://www.indian-grill.com/', '_blank', this.iabOptions);
                // browser.on('exit').subscribe(event => {
                //   console.log('IAB Event:', event);
                //   if (event.type == 'exit') {
                //     // App.exitApp();
                //     navigator['app'].exitApp();
                //   }
                // });
                browser.show();
                return [2 /*return*/];
            });
        });
    };
    HomePage.prototype.presentLoading = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Loading...',
                            duration: 5000
                        })];
                    case 1:
                        loading = _a.sent();
                        loading.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.ctorParameters = function () { return [
        { type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_2__["InAppBrowser"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
        { type: _services_api_service_service__WEBPACK_IMPORTED_MODULE_4__["ApiServiceService"] }
    ]; };
    HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./home.page.scss */ "./src/app/home/home.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_2__["InAppBrowser"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"], _services_api_service_service__WEBPACK_IMPORTED_MODULE_4__["ApiServiceService"]])
    ], HomePage);
    return HomePage;
}());



/***/ })

}]);
//# sourceMappingURL=home-home-module.js.map