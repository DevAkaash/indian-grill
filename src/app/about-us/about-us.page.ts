import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../services/api-service.service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.page.html',
  styleUrls: ['./about-us.page.scss'],
})
export class AboutUsPage implements OnInit {

  fetchedDATA: any;
  
  constructor(
    private api: ApiServiceService,
    private loadingCtrl: LoadingController
  ) { }

  ngOnInit() {
    this.getAbout();
  }

  async getAbout() {
    const loading = await this.loadingCtrl.create({});
    loading.present();

    this.api.aboutUSfn().then(result => {
      var aboutUS: any = result;
      console.log('About US: ', aboutUS);

      if (aboutUS.status == 200) {
        this.fetchedDATA = aboutUS;
        loading.dismiss();
      }
    }).catch(err => {
      console.log('err', err);
      loading.dismiss();
    })
  }

}
