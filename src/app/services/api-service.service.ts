import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {

  baseURL: any = 'https://www.indian-grill.com/wp-json/wl/v1';

  testimonial: any = '/testimonials';
  about: any = '/about';
  timing: any = '/timing';
  aboutUS: any = '/aboutus';
  contact: any = '/contact';
  address: any = '/address';

  constructor(
    private http: HttpClient
  ) { }

  /*Testimonial API*/
  public testimonialFn() {
    return new Promise((resolve, reject) => {
      this.http.get(this.baseURL + this.testimonial).subscribe(result => {
        resolve(result);
      }, error => {
        reject(error);
      });
    });
  }

  /*About HOME PAGE API*/
  public aboutFn() {
    return new Promise((resolve, reject) => {
      this.http.get(this.baseURL + this.about).subscribe(result => {
        resolve(result);
      }, error => {
        reject(error);
      });
    });
  }

  /*Restaurant Timing*/
  public timingFn() {
    return new Promise((resolve, reject) => {
      this.http.get(this.baseURL + this.timing).subscribe(result => {
        resolve(result);
      }, error => {
        reject(error);
      });
    });
  }

  /*About ABOUTUS PAGE API*/
  public aboutUSfn() {
    return new Promise((resolve, reject) => {
      this.http.get(this.baseURL + this.aboutUS).subscribe(result => {
        resolve(result);
      }, error => {
        reject(error);
      });
    });
  }

  /*Contact API*/
  public contactFn(param) {
    return new Promise((resolve, reject) => {
      this.http.post(this.baseURL + this.contact, param).subscribe(result => {
        resolve(result);
      }, error => {
        reject(error);
      });
    });
  }

  /*Address API*/
  public addressFn() {
    return new Promise((resolve, reject) => {
      this.http.get(this.baseURL + this.address).subscribe(result => {
        resolve(result);
      }, error => {
        reject(error);
      });
    });
  }
}
