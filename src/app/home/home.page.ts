import { Component, OnInit } from '@angular/core';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';

// import { Plugins } from '@capacitor/core';
import { LoadingController } from '@ionic/angular';
import { ApiServiceService } from '../services/api-service.service';
// const { App } = Plugins;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  iabOptions: InAppBrowserOptions = {
    location: 'no',
    usewkwebview: 'yes',
    footer: 'no',
    toolbar: 'no',
    zoom: 'no'
  };

  slideOpts = {
    initialSlide: 0,
    speed: 400
  };

  aboutDescription: any;
  testimonialData: any;
  resTimeTitle: any;
  resTimingData: any;

  constructor(private iab: InAppBrowser, private loadingCtrl: LoadingController, private api: ApiServiceService) {
    // setTimeout(() => this.inAppB(), 5000);
    // this.getAbout();
    // this.getTestimonial();
  }

  ngOnInit() {
    this.getAbout();
    this.getTestimonial();
    this.getRestaurantTiming();
  }

  async getAbout() {
    const loading = await this.loadingCtrl.create({});
    loading.present();

    this.api.aboutFn().then(result => {
      var aboutData: any = result;
      console.log('About: ', aboutData);

      if (aboutData.status == 200) {
        this.aboutDescription = aboutData.about;
        loading.dismiss();
      }
    }).catch(err => {
      console.log('err', err);
      loading.dismiss();
    });
  }

  async getTestimonial() {
    const loading = await this.loadingCtrl.create({});
    loading.present();

    this.api.testimonialFn().then(result => {
      var testimonial: any = result;
      console.log('Testimonials: ', testimonial);

      if (testimonial.status == 200) {
        this.testimonialData = testimonial.testimonial;
        loading.dismiss();
      }
    }).catch(err => {
      console.log('err', err);
      loading.dismiss();
    });
  }

  async getRestaurantTiming() {
    const loading = await this.loadingCtrl.create({});
    loading.present();

    this.api.timingFn().then(result => {
      var data: any = result;
      console.log('Timing: ', data);

      if (data.status == 200) {
        this.resTimeTitle = data.timing.title;
        this.resTimingData = data.timing.descp;
        loading.dismiss();
      }
    }).catch(err => {
      console.log('err', err);
      loading.dismiss();
    });
  }

  async inAppB() {
    const browser = this.iab.create('https://www.indian-grill.com/','_blank', this.iabOptions);
    // browser.on('exit').subscribe(event => {
    //   console.log('IAB Event:', event);
    //   if (event.type == 'exit') {
    //     // App.exitApp();
    //     navigator['app'].exitApp();
    //   }
    // });
    browser.show();
  }

  async presentLoading() {
    const loading = await this.loadingCtrl.create({
      message: 'Loading...',
      duration: 5000
    });
    loading.present();
  }

}
