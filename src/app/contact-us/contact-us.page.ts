import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoadingController, AlertController } from '@ionic/angular';
import { ApiServiceService } from '../services/api-service.service';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.page.html',
  styleUrls: ['./contact-us.page.scss'],
})
export class ContactUsPage implements OnInit {

  @ViewChild('contactUsForm', null) contactUsForm: NgForm;
  fetchedAddress: any = null;
  addressImage: any;
  address: any;
  
  constructor(
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private api: ApiServiceService
  ) { }

  ngOnInit() {
    this.getAddress();
  }

  onCheckValid() {
    return this.contactUsForm.value.comment && this.contactUsForm.value.comment.trim().length === 0;
  }

  async getAddress() {
    const loading = await this.loadingCtrl.create({});
    loading.present();

    this.api.addressFn().then(result => {
      this.fetchedAddress = result;

      if (this.fetchedAddress.status == 200) {
        this.addressImage = this.fetchedAddress.image;
        this.address = this.fetchedAddress.address;
        console.log('Address: ', this.fetchedAddress);
        loading.dismiss();
      }
    });
  }

  async onSubmit() {
    const loading = await this.loadingCtrl.create({
      message: 'Submitting Form...'
    });
    loading.present();

    var formBody = {
      name: this.contactUsForm.value.name,
      email: this.contactUsForm.value.email,
      tel: this.contactUsForm.value.telephone,
      message: this.contactUsForm.value.comment
    };

    console.log('Form Data: ', formBody);

    this.api.contactFn(formBody).then(result => {
      var fetchedData: any = result;
      console.log('Response: ', fetchedData);
      if (fetchedData.status == 200) {
        this.presentAlert('Information', fetchedData.message);
        loading.dismiss();
      }
    }).catch(err => {
      console.log(err);
      this.presentAlert('Alert', err.error.message);
      loading.dismiss();
    })
    // this.presentAlert('Information', 'Form submitted successfully!');
    // this.contactUsForm.reset();
  }

  /*Alert Service*/
  async presentAlert(fetchedHDR: String, fetchedMSG: String) {
    const alert = await this.alertCtrl.create({
      header: `${fetchedHDR}`,
      message: `${fetchedMSG}`,
      buttons: [
        {
          text: 'Okay',
          handler: () => {
            this.contactUsForm.reset();
            return;
          }
        }
      ]
    });
    alert.present();
  }

}
