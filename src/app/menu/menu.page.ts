import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  slideOpts = {
    initialSlide: 0,
    speed: 400
  };

  public menu: any[] = [
    {
      name: 'Veg Appetizers',
      menuItem: ['Veg Pakoda', 'Batata Vada', 'Allu Tikki Chaat', 'Samosa Chaat', 'Bhel'],
      isMenuOpen: true
    },
    {
      name: 'Non Veg Appetizers',
      menuItem: ['Chicken Tikka', 'Chicken Seekh Kebab', 'Chicken Malai Kebab', 'Tandoori Chicken', 'Chicken Mannchurian'],
      isMenuOpen: false
    },
    {
      name: 'Veg Entree',
      menuItem: ['Matar Paneer', 'Malai Kofta', 'Saag Paneer', 'Paneer Tikka Masala', 'Paneer Makhni'],
      isMenuOpen: false
    },
    {
      name: 'Non Veg Entree',
      menuItem: ['Chicken Tikka Masala', 'Chicken Makhani', 'Chilly Chicken Curry', 'Chicken Korma', 'Karahi Chicken'],
      isMenuOpen: false
    },
    {
      name: 'Desserts',
      menuItem: ['Fruit Custard', 'Kheer', 'Gulab Jamun', 'Rasmalai', 'Ice-Cream'],
      isMenuOpen: false
    },
  ];

  public toggleAccordion(menu) {
    console.log(menu);
    this.menu.map((listItem) => {
      if(menu == listItem){
        listItem.isMenuOpen = !listItem.isMenuOpen;
      } else {
        listItem.isMenuOpen = false;
      }
      return listItem;
    });
  }

  constructor() { }

  ngOnInit() {
  }

}
