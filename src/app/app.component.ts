import { Component } from '@angular/core';

import { Platform, MenuController, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';

// import { Plugins } from '@capacitor/core';
// const { App } = Plugins;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private iab: InAppBrowser,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private menuCtrl: MenuController,
    private navCtrl: NavController,
  ) {
    this.initializeApp();
    this.platform.backButton.subscribeWithPriority(-1, () => {
      // App.exitApp();
      navigator['app'].exitApp();
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  onMenuClose() {
    this.menuCtrl.close('m1');
  }

  onHome() {
    this.navCtrl.navigateRoot('/home');
  }

  onAboutUs() {
    this.navCtrl.navigateRoot('/about-us');
  }

  onContactUs() {
    this.navCtrl.navigateRoot('/contact-us');
  }

  // onOrderNow() {
  //   this.navCtrl.navigateRoot('/menu');
  // }

  async inAppB() {
    const iabOptions: InAppBrowserOptions = {
      location: 'no',
      usewkwebview: 'yes',
      footer: 'no',
      toolbar: 'no',
      usewkWebview: 'yes',
      zoom: 'no'
    };
    const browser = this.iab.create('https://www.indian-grill.com/','_blank', iabOptions);
    browser.show();
  }
}
